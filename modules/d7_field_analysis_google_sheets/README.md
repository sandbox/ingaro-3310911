# D7 field analysis module - Google Sheets extension

This tool generates a Google sheet listing Drupal bundles and fields for
a given entity type. It's aimed at making it easier to plan D7 to D9+
migrations.

## Prerequisites

This is a **Drupal 9** module, therefore you must have a working D9 site.
As well as its main database, that site expects another D7 database
to exist and be available using the `migrate` key.

You should have something like this in `settings.php`:

```php
// Main D9 database
$databases['default']['default'] = [ ... ];

// Migration database
$databases['migrate']['default'] = [
  'database' => 'name_of_d7_database',
  'username' => 'somebody',
  'password' => 'secret',
  'host' => 'db',
  'driver' => 'mysql',
];
```

## Create a Google Cloud project

Go to the Google Cloud console (https://console.cloud.google.com/iam-admin).

Create a project. I called mine "D7 field spreadsheet".

You need to enable the Google Sheets API for this project. You can do this
under APIs and services.

## Create a Google Cloud service account

A service account performs the role of someone editing a sheet, but via an
API instead of personally through the UI. You create the spreadsheet yourself,
but share it with the service user as you would with another person.
Drupal will act as the service user and populate the sheet.

Go to _service accounts_, and create a new account. I called mine "example".
Ignore _Grant this service account access to the project_ and
_Grant users access to this service account_.

This service account has its own email address. Based on the values above,
mine is `example@d7-field-spreadsheet.iam.gserviceaccount.com`.

We need to create an API key for the service account, which we then provide
to Drupal. Click on the service user's email address to edit it. On the
_keys_ tab you can add a new key. The type should be _JSON_.

You'll be able to download the key (a JSON file). Rename it
`d7_field_analysis_google_sheets.credentials.json` and put it
in the top level of Drupal's private files directory.

More info about service accounts:
https://cloud.google.com/iam/docs/service-accounts

## Create a Google sheet

Create a sheet in the usual way. Give it a title, but otherwise leave it
blank.

Share the sheet with the email address of the service user above.
Make them an editor.

## Populate the Google sheet

First get hold of the spreadsheet ID from its URL. It's the long string
after `/d`:

`https://docs.google.com/spreadsheets/d/` _long_string_here_ `/edit#gid=0`

Run the following command to populate the spreadsheet for a particular
entity type. You must create a separate sheet per entity type.

```
drush d7-field-analysis:google-sheet entityType spreadsheetId
```

**Note: run the command twice** - there's a known bug caused by the
sheet name changing from `Sheet1` to `Overview` mid-process.

## Editing the Google sheet

You can edit the Google sheet, but avoid any cells that were written
automatically. These should be locked anyway.

You can re-run the Drush command, and the sheet will update safely. But
bear in mind if the number and/or ordering of fields change, they will
move row, but your edits will not. The tool is not clever enough to move
rows around if that happens. If there are major changes, it's best to
generate a new sheet.

## Integration with d7_field_analysis

This tool integrates with the main d7_field_analysis module and show you
where field values are being used.

By default, the spreadsheet will contain links to the D9 site from which
you run the command. This is typically a private development site. If
you want to point to a different site, in order to share the spreadsheet,
modify the `d7_field_analysis_google_sheets.settings` configuration file.
There is no UI to do this, you need to edit the yml file by hand.
