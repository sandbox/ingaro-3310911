<?php

namespace Drupal\d7_field_analysis_google_sheets;

use Drupal\d7_field_analysis_google_sheets\Model\BundleInfo;
use Drupal\d7_field_analysis_google_sheets\Sheet\Helper;

abstract class BundleTask implements Task {

  protected string $bundleId;
  protected BundleInfo $bundle;
  protected string $sheetTitle;

  public function __construct(string $bundleId, BundleInfo $bundle) {
    $this->bundleId = $bundleId;
    $this->bundle = $bundle;
    $this->sheetTitle = Helper::safeIdentifier($bundleId);
  }

}
