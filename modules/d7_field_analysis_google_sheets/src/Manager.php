<?php

namespace Drupal\d7_field_analysis_google_sheets;

// TODO: make this a plugin manager and each entity type info a plugin

use Drupal\Core\Database\Database;
use Drupal\d7_field_analysis_google_sheets\Model\EntityTypeInfo;
use Drupal\d7_field_analysis_google_sheets\Model\FieldCollectionInfo;
use Drupal\d7_field_analysis_google_sheets\Model\NodeInfo;
use Drupal\d7_field_analysis_google_sheets\Model\ParagraphInfo;
use Drupal\d7_field_analysis_google_sheets\Model\VocabularyInfo;

class Manager {

  // TODO: replace this singleton with a proper service

  private static ?Manager $instance = NULL;

  public static function getInstance(): Manager {
    if (!self::$instance) {
      self::$instance = new Manager();
    }
    return self::$instance;
  }

  private function __construct() {
    $con = Database::getConnection('default', 'migrate');

    $this->entityTypes['node'] = new NodeInfo();
    if ($con->schema()->tableExists('taxonomy_term_data')) {
      $this->entityTypes['taxonomy_term'] = new VocabularyInfo();
    }
    if ($con->schema()->tableExists('paragraphs_item')) {
      $this->entityTypes['paragraphs_item'] = new ParagraphInfo();
    }
    if ($con->schema()->tableExists('field_collection_item')) {
      $this->entityTypes['field_collection_item'] = new FieldCollectionInfo();
    }

    foreach ($this->entityTypes as $entityTypeInfo) {
      $entityTypeInfo->loadBundles($con);
    }
  }



  /** @var EntityTypeInfo[] */
  protected $entityTypes = [];


  public function getEntityTypeInfo($id): ?EntityTypeInfo {
    return $this->entityTypes[$id];
  }

}
