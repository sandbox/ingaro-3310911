<?php

namespace Drupal\d7_field_analysis_google_sheets\Sheet;

use Drupal\d7_field_analysis_google_sheets\Model\EntityTypeInfo;
use Google\Client;
use Google\Service\Sheets;
use Google\Service\Sheets\BatchUpdateSpreadsheetRequest;
use Google\Service\Sheets\Request;
use Google\Service\Sheets\ValueRange;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;

class Builder {

  // Values to use with valueInputOption:
  const RAW = 'RAW';
  const USER_ENTERED = 'USER_ENTERED';


  use LoggerAwareTrait;

  /** @var \Google\Client */
  protected $client;

  /** @var \Google\Service\Sheets */
  protected $service;

  /** @var string */
  protected $spreadsheetId;

  /** @var EntityTypeInfo */
  protected $entityTypeInfo;

  /** @var \Psr\Log\LoggerInterface */
  protected $logger;

  /**
   * All the values to go into a BatchUpdateValuesRequest.
   *
   * @var array
   */
  protected $valueRanges = [];

  /**
   * All the requests to go into a BatchUpdateSpreadsheetRequest.
   *
   * @var array
   */
  protected $requests = [];


  protected SheetMapping $sheetMapping;


  function __construct(array $credentials, string $spreadsheetId, EntityTypeInfo $entityTypeInfo, LoggerInterface $logger) {
    $this->spreadsheetId = $spreadsheetId;
    $this->entityTypeInfo = $entityTypeInfo;

    $this->client = new Client();
    $this->client->setApplicationName('Google Sheets API PHP Quickstart');
    $this->client->setScopes(\Google_Service_Sheets::SPREADSHEETS);
    $this->client->setAuthConfig($credentials);

    $this->service = new Sheets($this->client);

    $this->setLogger($logger);
  }


}
