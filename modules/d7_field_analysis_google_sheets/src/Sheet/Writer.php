<?php

namespace Drupal\d7_field_analysis_google_sheets\Sheet;

use Drupal\d7_field_analysis_google_sheets\Task;
use Google\Client;
use Google\Service\Sheets;
use Google\Service\Sheets\BatchUpdateSpreadsheetRequest;
use Google\Service\Sheets\Request;
use Psr\Log\LoggerInterface;

class Writer {

  /** @var \Google\Client */
  protected $client;

  /** @var \Google\Service\Sheets */
  protected $service;

  /** @var string */
  protected $spreadsheetId;

  /** @var \Psr\Log\LoggerInterface */
  protected $logger;


  public function __construct(array $credentials, string $spreadsheetId, LoggerInterface $logger) {
    $this->spreadsheetId = $spreadsheetId;

    $this->client = new Client();
    $this->client->setApplicationName('Google Sheets API PHP Quickstart');
    $this->client->setScopes(\Google_Service_Sheets::SPREADSHEETS);
    $this->client->setAuthConfig($credentials);

    $this->service = new Sheets($this->client);
    $this->logger = $logger;
  }


  /**
   * @param Task[] $tasks
   */
  public function processTasks(array $tasks) {
    $sheetMapping = SheetMapping::load($this->service, $this->spreadsheetId);

    // TODO: need separate prerequisites for sheets and named ranges
//    foreach ($tasks as $task) {
//      $task->checkPrerequisites($sheetMapping);
//    }

    $this->createSheets($sheetMapping, $tasks);
    $this->createNamedRanges($sheetMapping, $tasks);
    $this->createProtectedRanges($sheetMapping, $tasks);

    $requests = [];
    $valueRanges = [];

    foreach ($tasks as $task) {
      $task->build($sheetMapping, $requests, $valueRanges);
    }

    if (!empty($requests)) {
      $this->service->spreadsheets->batchUpdate($this->spreadsheetId, new BatchUpdateSpreadsheetRequest([
        'requests' => $requests,
      ]));
    }

    if (!empty($valueRanges)) {
      $this->service->spreadsheets_values->batchUpdate($this->spreadsheetId, new Sheets\BatchUpdateValuesRequest([
        'valueInputOption' => 'USER_ENTERED',
        'data' => $valueRanges,
      ]));
    }
  }




  /**
   * Create any sheets that do not already exist.
   *
   * The mapping is updated accordingly.
   *
   * @param Task[] $tasks
   */
  protected function createSheets(SheetMapping $sheetMapping, array $tasks) {
    $requests = [];

    foreach ($tasks as $task) {
      foreach ($task->defineSheets() as $sheetTitle) {
        if (!$sheetMapping->hasSheetWithTitle($sheetTitle)) {
          $requests[] = new Request([
            'addSheet' => [
              'properties' => [
                'title' => $sheetTitle,
              ],
            ],
          ]);
        }
      }
    }

    if (!empty($requests)) {
      $response = $this->service->spreadsheets->batchUpdate($this->spreadsheetId, new BatchUpdateSpreadsheetRequest([
        'requests' => $requests,
      ]));
      foreach ($response->getReplies() as $reply) {
        $properties = $reply->getAddSheet()->getProperties();
        $sheetId = $properties->getSheetId();
        $title = $properties->getTitle();
        $this->logger->info("Created sheet $title with ID $sheetId");
        $sheetMapping->addSheet($sheetId, $title);
      }
    }
  }


  /**
   * Create any named ranges that do not already exist.
   *
   * The mapping is updated accordingly.
   *
   * @param Task[] $tasks
   */
  public function createNamedRanges(SheetMapping $sheetMapping, array $tasks) {
    $requests = [];

    foreach ($tasks as $task) {
      foreach ($task->defineNamedRanges($sheetMapping) as $name => $range) {
        if (!$sheetMapping->hasNamedRange($name)) {
          $requests[] = new Request([
            'addNamedRange' => [
              'namedRange' => [
                'name' => $name,
                'range' => $range,
              ],
            ],
          ]);
        }
      }
    }

    if (!empty($requests)) {
      $response = $this->service->spreadsheets->batchUpdate($this->spreadsheetId, new BatchUpdateSpreadsheetRequest([
        'requests' => $requests,
      ]));
      foreach ($response->getReplies() as $reply) {
        $namedRange = $reply->getAddNamedRange()->getNamedRange();
        $this->logger->info("A new named range called {$namedRange->getName()} with ID {$namedRange->getNamedRangeId()} was created");
        $sheetMapping->addNamedRange($namedRange);
      }
    }
  }



  /**
   * Create any protected ranges that do not already exist.
   *
   * The mapping is updated accordingly.
   *
   * @param Task[] $tasks
   */
  public function createProtectedRanges(SheetMapping $sheetMapping, array $tasks) {
    $requests = [];

    foreach ($tasks as $task) {
      foreach ($task->defineProtectedRanges() as $name) {
        if (!$sheetMapping->hasProtectedRange($name)) {
          if (!$sheetMapping->hasNamedRange($name)) {
            $this->logger->warning("A protected range with name $name was specified, but no such named range exists. Ignoring.");
            continue;
          }
          $requests[] = new Request([
            'addProtectedRange' => [
              'protectedRange' => [
                'namedRangeId' => $sheetMapping->getNamedRange($name)->getNamedRangeId(),
                'description' => 'This range is written to automatically.',
                'warningOnly' => TRUE,
              ],
            ],
          ]);
        }
      }
    }

    if (!empty($requests)) {
      $response = $this->service->spreadsheets->batchUpdate($this->spreadsheetId, new BatchUpdateSpreadsheetRequest([
        'requests' => $requests,
      ]));
      foreach ($response->getReplies() as $reply) {
        $protectedRange = $reply->getAddProtectedRange()->getProtectedRange();
        $this->logger->info("A new protected range with ID {$protectedRange->getProtectedRangeId()} was created. It corresponds to the named range with ID {$protectedRange->getNamedRangeId()}");
        $sheetMapping->addProtectedRange($protectedRange);
      }
    }
  }


}
