<?php

namespace Drupal\d7_field_analysis_google_sheets\Sheet;

use Google\Service\Sheets;
use Google\Service\Sheets\GridRange;
use Google\Service\Sheets\NamedRange;
use Google\Service\Sheets\ProtectedRange;

class SheetMapping {

  /**
   * @var array key=title, value=sheet ID
   */
  public array $sheetIds = [];

  /**
   * @var array key=ID, value=title
   */
  public array $sheetTitles = [];

  /** @var NamedRange[] key=ID, value=NamedRange */
  protected array $namedRanges = [];

  /** @var array key=name, value=ID */
  protected array $namedRangeIdsByName = [];

  /** @var ProtectedRange[] key=ID, value=ProtectedRange */
  protected array $protectedRanges = [];

  /** @var array key=name, value=ID */
  protected array $protectedRangeIdsByNamedRangeId = [];


  /**
   *
   */
  static function load(Sheets $service, string $spreadsheetId) {
    $mapping = new static();

    $spreadsheet = $service->spreadsheets->get($spreadsheetId);

    foreach ($spreadsheet->getNamedRanges() as $namedRange) {
      $mapping->namedRanges[$namedRange->getNamedRangeId()] = $namedRange;
      $mapping->namedRangeIdsByName[$namedRange->getName()] = $namedRange->getNamedRangeId();
    }

    foreach ($spreadsheet->getSheets() as $sheet) {
      $sheetId = $sheet->properties['sheetId'];
      $title = $sheet->properties['title'];
      $mapping->sheetTitles[$sheetId] = $title;
      $mapping->sheetIds[$title] = $sheetId;

      foreach ($sheet->getProtectedRanges() as $protectedRange) {
        if ($protectedRange->getNamedRangeId()) {
          $mapping->protectedRanges[$protectedRange->getProtectedRangeId()] = $protectedRange;
          $mapping->protectedRangeIdsByNamedRangeId[$protectedRange->getNamedRangeId()] = $protectedRange->getProtectedRangeId();
        }
      }
    }

    return $mapping;
  }


  function hasOverviewSheet(): bool {
    return isset($this->sheetTitles['0']);
  }

  function hasBundleSheet(string $bundleId): bool {
    $title = self::sheetTitleForBundleId($bundleId);
    return $this->hasSheetWithTitle($title);
  }

  function getBundleSheetId(string $bundleId): ?int {
    $title = self::sheetTitleForBundleId($bundleId);
    return $title ? $this->getSheetId(self::sheetTitleForBundleId($bundleId)) : NULL;
  }

  function getSheetId(string $title): int {
    if (isset($this->sheetIds[$title])) {
      return $this->sheetIds[$title];
    }
    else {
      throw new \RuntimeException("Unknown sheet title: $title");
    }
  }

  function hasSheetWithTitle(string $title): bool {
    return !empty($this->sheetIds[$title]);
  }

  function getSheetTitle(int $sheetId): string {
    if (isset($this->sheetTitles[$sheetId])) {
      return $this->sheetTitles[$sheetId];
    }
    else {
      throw new \RuntimeException("Unknown sheet ID: $sheetId");
    }
  }

  static function sheetTitleForBundleId(string $bundleId): string {
    return Helper::safeIdentifier($bundleId);
  }


  function addSheet(int $sheetId, string $title) {
    if (isset($this->sheetTitles[$sheetId])) {
      throw new \RuntimeException("Mapping already contains a sheet called $title");
    }
    if (isset($this->sheetIds[$sheetId])) {
      throw new \RuntimeException("Mapping already contains a sheet with ID $sheetId");
    }
    $this->sheetTitles[$sheetId] = $title;
    $this->sheetIds[$title] = $sheetId;
  }


  function hasNamedRange(string $name): bool {
    return isset($this->namedRangeIdsByName[$name]);
  }

  function getNamedRange(string $name): NamedRange {
    $nid = $this->namedRangeIdsByName[$name] ?? 0;
    $namedRange = $this->namedRanges[$nid];
    if (!$namedRange) {
      throw new \RuntimeException("Unknown named range: $name");
    }
    return $namedRange;
  }

  function a1Notation(GridRange $range): string {
    $title = $this->getSheetTitle($range->sheetId);
    $firstColumn = Helper::columnIndexToLetters($range->startColumnIndex);
    $lastColumn = Helper::columnIndexToLetters($range->endColumnIndex - 1);
    $firstRow = $range->startRowIndex + 1;
    $lastRow = $range->endRowIndex;
    return "{$title}!{$firstColumn}{$firstRow}:{$lastColumn}{$lastRow}";
  }


  function addNamedRange(NamedRange $namedRange) {
    $this->namedRanges[$namedRange->getNamedRangeId()] = $namedRange;
    $this->namedRangeIdsByName[$namedRange->getName()] = $namedRange->getNamedRangeId();
  }


  function hasProtectedRange(string $name): bool {
    $nid = $this->namedRangeIdsByName[$name] ?? 0;
    $pid = $this->protectedRangeIdsByNamedRangeId[$nid] ?? 0;
    return isset($this->protectedRanges[$pid]);
  }

  function addProtectedRange(ProtectedRange $protectedRange) {
    $this->protectedRanges[$protectedRange->getProtectedRangeId()] = $protectedRange;
    if ($protectedRange->getNamedRangeId()) {
      $this->protectedRangeIdsByNamedRangeId[$protectedRange->getNamedRangeId()] = $protectedRange->getProtectedRangeId();
    }
  }


  function getOverviewHyperlink(string $text) {
    return "=HYPERLINK(\"#gid=0\", \"{$text}\")";
  }

  function getSheetHyperlink(int $sheetId, string $text) {
    return "=HYPERLINK(\"#gid=$sheetId\", \"{$text}\")";
  }

}
