<?php

namespace Drupal\d7_field_analysis_google_sheets\Sheet;

use Google\Service\Sheets\GridRange;
use Google\Service\Sheets\NamedRange;
use Google\Service\Sheets\Request;

/**
 * Miscellaneous functions for common tasks.
 */
class Helper {

  const TOP = 'TOP';
  const MIDDLE = 'MIDDLE';
  const BOTTOM = 'BOTTOM';
  const LEFT = 'LEFT';
  const CENTER = 'CENTER';
  const RIGHT = 'RIGHT';


  // Don't let tab names (or named regions) start with a number.
  public static function safeIdentifier(string $identifier): string {
    if (preg_match('/^[0-9]/', $identifier)) {
      return "_$identifier";
    }
    else {
      return $identifier;
    }
  }


  /**
   * Turns 1 into "A", 2 into "B", 27 into "AA" etc.
   *
   * @param int $n
   * @return string
   */
  static function columnIndexToLetters(int $n) {
    $remainder = $n % 26;
    $quotient = intdiv($n, 26);
    $c = chr($remainder + 65);
    return ($quotient > 0)
      ? self::columnIndexToLetters($quotient - 1) . $c
      : $c;
  }



  static function makeRequestToResizeColumn(string $sheetId, int $columnIndex, int $pixelSize) {
    return new Request([
      'updateDimensionProperties' => [
        'range' => [
          'sheetId' => $sheetId,
          'dimension' => 'COLUMNS',
          'startIndex' => $columnIndex,
          'endIndex' => $columnIndex + 1,
        ],
        'properties' => [
          'pixelSize' => $pixelSize,
        ],
        'fields' => 'pixelSize',
      ],
    ]);
  }

  static function makeRequestToResizeRow(string $sheetId, int $rowIndex, int $pixelSize) {
    return new Request([
      'updateDimensionProperties' => [
        'range' => [
          'sheetId' => $sheetId,
          'dimension' => 'ROWS',
          'startIndex' => $rowIndex,
          'endIndex' => $rowIndex + 1,
        ],
        'properties' => [
          'pixelSize' => $pixelSize,
        ],
        'fields' => 'pixelSize',
      ],
    ]);
  }

  static function makeRequestToSetVerticalAlignmentForSheet(string $sheetId, string $alignment) {
    return new Request([
      'repeatCell' => [
        'range' => [
          'sheetId' => $sheetId,
        ],
        'cell' => [
          'userEnteredFormat' => [
            'verticalAlignment' => $alignment,
          ],
        ],
        'fields' => 'userEnteredFormat(verticalAlignment)',
      ],
    ]);
  }

  static function makeRequestToSetHorizontalAlignmentForColumn(string $sheetId, int $columnIndex, string $alignment) {
    return new Request([
      'repeatCell' => [
        'range' => [
          'sheetId' => $sheetId,
          'startRowIndex' => 0,
          'startColumnIndex' => $columnIndex,
          'endColumnIndex' => $columnIndex + 1,
        ],
        'cell' => [
          'userEnteredFormat' => [
            'horizontalAlignment' => $alignment,
          ],
        ],
        'fields' => 'userEnteredFormat(horizontalAlignment)',
      ],
    ]);
  }

  static function makeRequestToSetWrapStrategyForColumn(string $sheetId, int $columnIndex, string $wrapStrategy) {
    return new Request([
      'repeatCell' => [
        'range' => [
          'sheetId' => $sheetId,
          'startRowIndex' => 0,
          'startColumnIndex' => $columnIndex,
          'endColumnIndex' => $columnIndex + 1,
        ],
        'cell' => [
          'userEnteredFormat' => [
            'wrapStrategy' => $wrapStrategy,
          ],
        ],
        'fields' => 'userEnteredFormat(wrapStrategy)',
      ],
    ]);
  }

  static function makeRequestToFormatRowAsHeader(int $sheetId, int $rowIndex) {
    return new Request([
      'repeatCell' => [
        'range' => [
          'sheetId' => $sheetId,
          'startRowIndex' => $rowIndex,
          'endRowIndex' => $rowIndex + 1,
          'startColumnIndex' => 0,
        ],
        'cell' => [
          'userEnteredFormat' => [
            'backgroundColor' => ['red' => 0.8, 'green' => 0.8, 'blue' => 0.8],
            'textFormat' => [
              'bold' => TRUE,
            ],
            'verticalAlignment' => Helper::MIDDLE,
          ],
        ],
        'fields' => 'userEnteredFormat(textFormat,backgroundColor,verticalAlignment)',
      ],
    ]);
  }

  static function makeRequestToFormatRangeAsHeader(GridRange $range) {
    return new Request([
      'repeatCell' => [
        'range' => (array) $range->toSimpleObject(),
        'cell' => [
          'userEnteredFormat' => [
            'backgroundColor' => ['red' => 0.8, 'green' => 0.8, 'blue' => 0.8],
            'textFormat' => [
              'bold' => TRUE,
            ],
            'verticalAlignment' => Helper::MIDDLE,
          ],
        ],
        'fields' => 'userEnteredFormat(textFormat,backgroundColor,verticalAlignment)',
      ],
    ]);
  }

  static function makeRequestToSetRowBorder(int $sheetId, int $rowIndex, string $border) {
    return new Request([
      'updateBorders' => [
        'range' => [
          'sheetId' => $sheetId,
          'startRowIndex' => $rowIndex,
          'endRowIndex' => $rowIndex + 1,
          'startColumnIndex' => 0,
        ],
        $border => [
          'style' => 'SOLID_THICK',
          'color' => ['red' => 0.4, 'green' => 0.4, 'blue' => 0.4],
        ],
      ],
    ]);
  }

  static function makeRequestToMergeCellsVertically(int $sheetId, int $startRowIndex, int $endRowIndex, int $columnIndex) {
    return new Request([
      'mergeCells' => [
        'range' => [
          'sheetId' => $sheetId,
          'startRowIndex' => $startRowIndex,
          'endRowIndex' => $endRowIndex,
          'startColumnIndex' => $columnIndex,
          'endColumnIndex' => $columnIndex + 1,
        ],
        'mergeType' => 'MERGE_COLUMNS',
      ],
    ]);
  }

}
