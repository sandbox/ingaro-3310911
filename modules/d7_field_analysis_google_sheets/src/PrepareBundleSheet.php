<?php

namespace Drupal\d7_field_analysis_google_sheets;

use Drupal\d7_field_analysis_google_sheets\Sheet\Helper;
use Drupal\d7_field_analysis_google_sheets\Sheet\SheetMapping;
use Google\Service\Sheets\Request;
use Google\Service\Sheets\ValueRange;

class PrepareBundleSheet extends BundleTask {

  use DefaultTaskTrait;


  public function defineSheets(): array {
    return [
      $this->sheetTitle,
    ];
  }

  public function defineNamedRanges(SheetMapping $sheetMapping): array {
    $sheetId = $sheetMapping->getSheetId($this->sheetTitle);

    return [
      "{$this->sheetTitle}.overview_link_cell" => [
        'sheetId' => $sheetId,
        'startRowIndex' => 0,
        'endRowIndex' => 1,
        'startColumnIndex' => 0,
        'endColumnIndex' => 1,
      ],
      "{$this->sheetTitle}.title_cell" => [
        'sheetId' => $sheetId,
        'startRowIndex' => 1,
        'endRowIndex' => 2,
        'startColumnIndex' => 0,
        'endColumnIndex' => 1,
      ],
      "{$this->sheetTitle}.column_headings_row" => [
        'sheetId' => $sheetId,
        'startRowIndex' => 2,
        'endRowIndex' => 3,
        'startColumnIndex' => 0,
      ],
    ];
  }

  public function defineProtectedRanges(): array {
    return [
      "{$this->sheetTitle}.overview_link_cell",
      "{$this->sheetTitle}.title_cell",
    ];
  }


  public function build(SheetMapping $sheetMapping, array &$requests, array &$valueRanges): void {
    $sheetId = $sheetMapping->getSheetId($this->sheetTitle);

    // Set up column widths & alignment.
    $requests[] = Helper::makeRequestToSetVerticalAlignmentForSheet($sheetId, Helper::TOP);
    $requests[] = Helper::makeRequestToResizeColumn($sheetId, 0, 250);  // label
    $requests[] = Helper::makeRequestToResizeColumn($sheetId, 1, 200);  // machine name
    $requests[] = Helper::makeRequestToResizeColumn($sheetId, 2, 100);  // type
    $requests[] = Helper::makeRequestToResizeColumn($sheetId, 3, 100);  // property
    $requests[] = Helper::makeRequestToResizeColumn($sheetId, 4, 270);  // references
    $requests[] = Helper::makeRequestToResizeColumn($sheetId, 5, 80);   // usage
    $requests[] = Helper::makeRequestToSetHorizontalAlignmentForColumn($sheetId, 5, Helper::CENTER);
    $requests[] = Helper::makeRequestToResizeColumn($sheetId, 6, 80);   // cardinality
    $requests[] = Helper::makeRequestToSetHorizontalAlignmentForColumn($sheetId, 6, Helper::CENTER);
    $requests[] = Helper::makeRequestToResizeColumn($sheetId, 7, 80);   // required
    $requests[] = Helper::makeRequestToSetHorizontalAlignmentForColumn($sheetId, 7, Helper::CENTER);
    $requests[] = Helper::makeRequestToResizeColumn($sheetId, 8, 80);   // migrate?
    $requests[] = Helper::makeRequestToSetHorizontalAlignmentForColumn($sheetId, 8, Helper::CENTER);
    $requests[] = Helper::makeRequestToResizeColumn($sheetId, 9, 80);   // migrate done?
    $requests[] = Helper::makeRequestToSetHorizontalAlignmentForColumn($sheetId, 9, Helper::CENTER);
    $requests[] = Helper::makeRequestToResizeColumn($sheetId, 10, 300);  // notes
    $requests[] = Helper::makeRequestToResizeColumn($sheetId, 11, 250); // label
    $requests[] = Helper::makeRequestToResizeColumn($sheetId, 12, 200); // machine name
    $requests[] = Helper::makeRequestToResizeColumn($sheetId, 13, 200); // type
    $requests[] = Helper::makeRequestToResizeColumn($sheetId, 14, 80);  // cardinality
    $requests[] = Helper::makeRequestToSetHorizontalAlignmentForColumn($sheetId, 14, Helper::CENTER);
    $requests[] = Helper::makeRequestToResizeColumn($sheetId, 15, 80);  // required
    $requests[] = Helper::makeRequestToSetHorizontalAlignmentForColumn($sheetId, 15, Helper::CENTER);

    $this->buildRowWithOverviewLink($sheetMapping, $requests, $valueRanges);
    $this->buildRowWithTitle($sheetMapping, $requests, $valueRanges);
    $this->buildRowForColumnHeadings($sheetMapping, $requests, $valueRanges);
  }


  function buildRowWithOverviewLink(SheetMapping $sheetMapping, array &$requests, array &$valueRanges) {
    $range = $sheetMapping->getNamedRange("{$this->sheetTitle}.overview_link_cell")->getRange();

    // Formatting.
    $requests[] = Helper::makeRequestToResizeRow($range->sheetId, $range->startRowIndex, 40);

    $requests[] = new Request([
      'repeatCell' => [
        'range' => (array) $range->toSimpleObject(),
        'cell' => [
          'userEnteredFormat' => [
            'textFormat' => [
              'bold' => TRUE,
            ],
          ],
        ],
        'fields' => 'userEnteredFormat(textFormat)',
      ],
    ]);

    // Populate cells.
    $text = $sheetMapping->getOverviewHyperlink('Go to overview');
    $valueRanges[] = new ValueRange([
      'range' => $sheetMapping->a1Notation($range),
      'values' => [[$text]],
    ]);
  }


  function buildRowWithTitle(SheetMapping $sheetMapping, array &$requests, array &$valueRanges) {
    $range = $sheetMapping->getNamedRange("{$this->sheetTitle}.title_cell")->getRange();

    // Make the whole row a single cell.
    $requests[] = new Request([
      'mergeCells' => [
        'range' => [
          'sheetId' => $range->sheetId,
          'startRowIndex' => $range->startRowIndex,
          'endRowIndex' => $range->endRowIndex,
          'startColumnIndex' => 0,
          'endColumnIndex' => PrepareBundleSourceColumns::NUMBER_OF_COLUMNS,
        ],
        'mergeType' => 'MERGE_ALL',
      ],
    ]);

    // Formatting.
    $requests[] = new Request([
      'repeatCell' => [
        'range' => (array) $range->toSimpleObject(),
        'cell' => [
          'userEnteredFormat' => [
            'textFormat' => [
              'fontSize' => 18,
              'bold' => TRUE,
            ],
            'verticalAlignment' => Helper::MIDDLE,
          ],
        ],
        'fields' => 'userEnteredFormat(textFormat,verticalAlignment)',
      ],
    ]);

    $requests[] = Helper::makeRequestToResizeRow($range->sheetId, $range->startRowIndex, 60);

    // Populate cells.
    $text = "{$this->bundle->label} ({$this->bundle->name})";
    $valueRanges[] = new ValueRange([
      'range' => $sheetMapping->a1Notation($range),
      'values' => [[$text]],
    ]);
  }


  // Set up the row for column headings, but don't populate it. This task doesn't know what each column is for.

  function buildRowForColumnHeadings(SheetMapping $sheetMapping, array &$requests, array &$valueRanges) {
    $range = $sheetMapping->getNamedRange("{$this->sheetTitle}.column_headings_row")->getRange();

    $requests[] = Helper::makeRequestToResizeRow($range->sheetId, $range->startRowIndex, 30);
    $requests[] = Helper::makeRequestToFormatRangeAsHeader($range);
    $requests[] = Helper::makeRequestToSetRowBorder($range->sheetId, $range->startRowIndex, 'bottom');
  }


}
