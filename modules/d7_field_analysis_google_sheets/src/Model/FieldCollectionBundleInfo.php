<?php

namespace Drupal\d7_field_analysis_google_sheets\Model;

use Drupal\Core\Database\Connection;

/**
 * Field collection is a special case, because the 'bundle' name is not unique.
 */

class FieldCollectionBundleInfo extends BundleInfo {

  public string $parentEntityType;
  public string $parentFieldName;

  function __construct(Connection $con, string $entityType, string $parentEntityType, string $parentFieldName) {
    $this->parentEntityType = $parentEntityType;
    $this->parentFieldName = $parentFieldName;
    parent::__construct($con, $entityType, $parentFieldName);
  }

}
