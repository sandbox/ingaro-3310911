<?php

namespace Drupal\d7_field_analysis_google_sheets\Model;

use Drupal\Core\Database\Connection;

class NodeInfo extends EntityTypeInfo implements SupportsEntityReferenceField {

  use StandardBundleUsageCalculation;


  /**
   * {@inheritdoc}
   */
  public function loadBundles(Connection $con) {
    $sql = 'select t.type, t.name, (select count(nid) from {node} n where n.type=t.type) as qty from {node_type} t order by t.name';
    $results = $con->query($sql)
      ->fetchAll();

    foreach ($results as $result) {
      $bundle = new BundleInfo($con, $this->getEntityTypeId(), $result->type);
      $bundle->label = $result->name;
      $bundle->quantity = $result->qty;
      $this->bundles[$result->type] = $bundle;
    }
  }

  public function getEntityTypeId(): string {
    return 'node';
  }

  public function getLabel(): string {
    return 'Node';
  }

  public function getTable(): string {
    return 'node';
  }

  public function getIdColumn(): string {
    return 'nid';
  }

  public function getBundleColumn(): string {
    return 'type';
  }

}
