<?php

namespace Drupal\d7_field_analysis_google_sheets\Model;

use Drupal\Core\Database\Connection;

/**
 * For entities with a simple table, we can use standardised queries for calculating usage etc.
 *
 * TODO: what shall we call this?
 */
trait StandardBundleUsageCalculation {

  /**
   * @param Connection $con
   * @param FieldInstanceInfo $field
   * @return array
   *   Array whose keys are bundle names and values are quantities.
   */
  public function loadPerBundleUsageForEntityReferenceField(Connection $con, FieldInstanceInfo $field): array {

    // The SQL query looks something like this:
    // SELECT n.type, COUNT(n.nid)
    // FROM field_data_somefield f
    // INNER JOIN node n ON n.nid=f.somefield_target_id
    // WHERE f.entity_type='sometype' AND f.bundle='somebundle'
    // GROUP BY n.type

    $fieldDataTable = "field_data_{$field->fieldName}";
    $fieldDataTargetIdColumn = "{$field->fieldName}_target_id";
    $entityTable = $this->getTable();
    $entityIdColumn = $this->getIdColumn();
    $entityBundleColumn = $this->getBundleColumn();

    $query = $con->select($fieldDataTable, 'f');
    $query->addField('e', $entityBundleColumn);
    $query->addExpression("COUNT(e.{$entityIdColumn})");
    $query->innerJoin($entityTable, 'e', "e.{$entityIdColumn}=f.{$fieldDataTargetIdColumn}");
    $query->condition('f.entity_type', $field->entityType);
    $query->condition('f.bundle', $field->bundle);
    $query->groupBy("e.{$entityBundleColumn}");
    return $query->execute()->fetchAllKeyed();
  }

  public abstract function getTable(): string;
  public abstract function getIdColumn(): string;
  public abstract function getBundleColumn(): string;

}
