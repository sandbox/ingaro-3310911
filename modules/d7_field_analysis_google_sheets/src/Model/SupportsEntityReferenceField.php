<?php

namespace Drupal\d7_field_analysis_google_sheets\Model;

use Drupal\Core\Database\Connection;

interface SupportsEntityReferenceField {

  /**
   *
   *
   * @param Connection $con
   * @param FieldInstanceInfo $field
   * @return array
   *   Array whose keys are bundle names and values are quantities.
   */
  public function loadPerBundleUsageForEntityReferenceField(Connection $con, FieldInstanceInfo $field): array;

}
