<?php

namespace Drupal\d7_field_analysis_google_sheets\Model;

use Drupal\Core\Database\Connection;

class FieldCollectionInfo extends EntityTypeInfo implements ChildEntityInfoInterface {

  use ChildEntityInfoTrait;


  /**
   * {@inheritdoc}
   */
  public function loadBundles(Connection $con) {
    $sql = "select distinct i.field_name, i.entity_type from field_config_instance i inner join field_config f on f.id=i.field_id where f.type='field_collection' order by i.entity_type, i.field_name";
    $results = $con->query($sql)
      ->fetchAll();

    foreach ($results as $result) {
      // Because field collection "bundle" names are not unique,
      // we use both the field's entity type and the field name
      // to form a unique identifier.
      $id = "{$result->entity_type}.{$result->field_name}";

      $bundle = new FieldCollectionBundleInfo($con, $this->getEntityTypeId(), $result->entity_type, $result->field_name);
      $bundle->label = "{$result->entity_type} {$result->field_name}";
      $bundle->quantity = self::loadQuantity($con, $result->entity_type, $result->field_name);
      $this->bundles[$id] = $bundle;
    }
  }

  public function getEntityTypeId(): string {
    return 'field_collection_item';
  }

  public function getLabel(): string {
    return 'Field collection';
  }

  public function entityTable(): string {
    return 'field_collection_item';
  }

  public function entityTableIdColumn(): string {
    return 'item_id';
  }

  public function entityTableBundleColumn(): string {
    return 'field_name';
  }



  static function loadQuantity(Connection $con, string $entityType, string $fieldName): int {
    $fieldDataTable = "field_data_{$fieldName}";

    $query = $con->select($fieldDataTable, 'f');
    $query->addExpression('count(*)', 'quantity');
    $query->condition('f.entity_type', $entityType);
    $values = $query->execute()->fetchCol();
    $value = reset($values);
    return $value;
  }


  function loadUsage(Connection $con) {

    $query = $con->select('field_config_instance', 'i');
    $query->fields('i', ['entity_type', 'bundle', 'field_name']);
    $query->innerJoin('field_config', 'f', 'f.id=i.field_id');
    $query->condition('f.type', 'field_collection');

    foreach ($query->execute()->fetchAll() as $row) {
      $bundleId = "{$row->entity_type}.{$row->field_name}";
      $source = "{$row->entity_type}.{$row->bundle}.{$row->field_name}";
      $this->inboundUsage[$bundleId][$source] = 0;
    }
  }

}
