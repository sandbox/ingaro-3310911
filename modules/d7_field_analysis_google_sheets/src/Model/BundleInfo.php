<?php

namespace Drupal\d7_field_analysis_google_sheets\Model;

use Drupal\Core\Database\Connection;

class BundleInfo {

  public string $entityType;
  public string $name;
  public string $label;
  public int $quantity;

  /** @var FieldInstanceInfo[] */
  public array $fieldInstances = [];

  /**
   * Array of entity reference fields that can refer to entities of this bundle.
   *
   * @var FieldInstanceInfo[]
   */
  public array $usage = [];

  function __construct(Connection $con, string $entityType, string $name) {
    $this->entityType = $entityType;
    $this->name = $name;
    $this->fieldInstances = FieldInstanceInfo::load($con, $entityType, $name);
  }

  function __toString() {
    return "$this->entityType.$this->name";
  }


}
