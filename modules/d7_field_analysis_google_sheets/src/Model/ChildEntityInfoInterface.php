<?php

namespace Drupal\d7_field_analysis_google_sheets\Model;

use Drupal\Core\Database\Connection;

interface ChildEntityInfoInterface {

  function loadUsage(Connection $con);


  /*
   * Key = string (bundleId)
   * Value = array, whose keys are identifiers to fields, of the form entityType.bundle.fieldName
   *                and values are the number of such field values
   */

  /**
   * Get usage data for a given bundle.
   *
   * @param string $bundleId
   *   As returned by getBundles().
   * @return array
   *   Keys are identifiers to fields, in the form entityType.bundle.fieldName
   *   Values are the number of such field values
   */
  public function getInboundUsage(string $bundleId): array;

}
