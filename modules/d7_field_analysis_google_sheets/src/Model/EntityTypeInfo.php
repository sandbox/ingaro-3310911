<?php

namespace Drupal\d7_field_analysis_google_sheets\Model;

use Drupal\Core\Database\Connection;
use RuntimeException;

abstract class EntityTypeInfo {

  /**
   * This entity type's bundles, keyed by a string identifier.
   *
   * The identifier varies between entity types, but it can be used in
   * situations where a unique value is needed for each bundle, such
   * as a Google sheet title.
   *
   * @var BundleInfo[]
   */
  public array $bundles = [];

  /**
   * Called by the manager. Populates $this->bundles.
   *
   * Subclasses should override this.
   */
  public abstract function loadBundles(Connection $con);


  public abstract function getEntityTypeId(): string;
  public abstract function getLabel(): string;


  /**
   * @return BundleInfo[]
   */
  public function getBundles(): array {
    return $this->bundles;
  }

  public function getBundleInfo($id): BundleInfo {
    if (isset($this->bundles[$id])) {
      return $this->bundles[$id];
    }
    else {
      throw new RuntimeException("Requested a non-existent bundle ($id) from the {$this->getEntityTypeId()} entity type");
    }
  }

}
