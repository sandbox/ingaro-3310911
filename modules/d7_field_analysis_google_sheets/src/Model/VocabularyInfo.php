<?php

namespace Drupal\d7_field_analysis_google_sheets\Model;

use Drupal\Core\Database\Connection;

class VocabularyInfo extends EntityTypeInfo implements SupportsEntityReferenceField {

  public function getEntityTypeId(): string {
    return 'taxonomy_term';
  }

  public function getLabel(): string {
    return 'Term';
  }

  public function entityTable(): string {
    return 'taxonomy_term_data';
  }

  public function entityTableIdColumn(): string {
    return 'tid';
  }

  public function entityTableBundleColumn(): string {
    return 'vid';
  }

  /**
   * {@inheritdoc}
   */
  public function loadBundles(Connection $con) {
    $sql = 'select v.machine_name, v.name, (select count(t.tid) from {taxonomy_term_data} t where t.vid=v.vid) as qty from {taxonomy_vocabulary} v order by v.machine_name';
    $results = $con->query($sql)
      ->fetchAll();

    foreach ($results as $result) {
      $bundle = new BundleInfo($con, $this->getEntityTypeId(), $result->machine_name);
      $bundle->label = $result->name;
      $bundle->quantity = $result->qty;
      $this->bundles[$result->machine_name] = $bundle;
    }
  }


  /**
   *
   *
   * @param Connection $con
   * @param FieldInstanceInfo $field
   * @return array
   *   Array whose keys are bundle names and values are quantities.
   */
  public function loadPerBundleUsageForEntityReferenceField(Connection $con, FieldInstanceInfo $field): array {

    // The SQL query looks something like this:
    // SELECT v.machine_name, COUNT(t.tid)
    // FROM field_data_somefield f
    // INNER JOIN taxonomy_term_data t ON t.tid=f.somefield_tid
    // INNER JOIN taxonomy_vocabulary v ON v.vid=t.vid
    // WHERE f.entity_type='node' AND f.bundle='somebundle'
    // GROUP BY v.machine_name

    $fieldDataTable = "field_data_{$field->fieldName}";
    $fieldDataTargetIdColumn = "{$field->fieldName}_target_id";

    $query = $con->select($fieldDataTable, 'f');
    $query->addField('v', 'machine_name');
    $query->addExpression("COUNT(t.tid)");
    $query->innerJoin('taxonomy_term_data', 't', "t.tid=f.{$fieldDataTargetIdColumn}");
    $query->innerJoin('taxonomy_vocabulary', 'v', 'v.vid=t.vid');
    $query->condition('f.entity_type', $field->entityType);
    $query->condition('f.bundle', $field->bundle);
    $query->groupBy("v.machine_name");
    return $query->execute()->fetchAllKeyed();
  }


}
