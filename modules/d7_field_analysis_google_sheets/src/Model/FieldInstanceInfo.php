<?php

namespace Drupal\d7_field_analysis_google_sheets\Model;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Database;
use Drupal\d7_field_analysis_google_sheets\Manager;

/**
 * Represents a field instance, ie one or more rows in the Google sheet.
 */
class FieldInstanceInfo {

  public string $entityType;
  public string $bundle;
  public string $fieldName;
  public string $type;
  public int $cardinality;
  public $instanceData;
  public $fieldData;

  // Used for fields that reference other entities.
  // Key=bundle name, value=quantity
  public $references = [];

  // key=bundle name, value=quantity
  public array $childParagraphs = [];


  static function load(Connection $con, string $entityType, string $bundle): array {

    $query = $con->select('field_config_instance', 'i');
    $query->addField('i', 'data', 'instance_data');
    $query->addJoin('inner', 'field_config', 'f', 'i.field_id=f.id');
    $query->addField('f', 'type');
    $query->addField('f', 'field_name');
    $query->addField('f', 'cardinality');
    $query->addField('f', 'data', 'field_data');
    $query->condition('i.entity_type', $entityType);
    $query->condition('i.bundle', $bundle);
    $query->condition('i.deleted', FALSE);
    $rows = $query->execute()->fetchAll();

    $fields = [];
    foreach ($rows as $row) {
      $field = new FieldInstanceInfo();
      $field->entityType = $entityType;
      $field->bundle = $bundle;
      $field->fieldName = $row->field_name;
      $field->type = $row->type;
      $field->cardinality = $row->cardinality;
      $field->instanceData = unserialize($row->instance_data);
      $field->fieldData = unserialize($row->field_data);
      $field->loadChildParagraphs($con);
      $fields[] = $field;
    }

    return $fields;
  }


  public static function headerOutput() {
    return [
      'Label', 'Machine name', 'Type', 'Property', 'References', 'Usage', 'Cardinality', 'Required',
    ];
  }

  /**
   * @return array[]
   *   Something that can be used as the 'values' property of a ValueRange.
   */

  // TODO: move this to Builder?

  public function toGoogleSheetValues(): array {
    $primaryRow = [];
    $primaryRow[] = $this->instanceData['label'];
    $primaryRow[] = $this->fieldName;
    $primaryRow[] = $this->expandedType();
    $primaryRow[] = '';
    $primaryRow[] = $this->references();
    $primaryRow[] = $this->usage();
    $primaryRow[] = $this->expandedCardinality();
    $primaryRow[] = $this->instanceData['required'] ? 'required' : '-';

    $secondaryRows = [];

    if ($this->type === 'text_with_summary') {
      $primaryRow[3] = 'value';
      $secondaryRows[] = ['', '', '', 'summary', '', '', ''];
      $secondaryRows[] = ['', '', '', 'format', '', '', ''];
    }
    if ($this->type === 'text_long') {
      $primaryRow[3] = 'value';
      $secondaryRows[] = ['', '', '', 'format', '', '', ''];
    }

    $rows = [];
    $rows[] = $primaryRow;
    array_push($rows, ...$secondaryRows);
    return $rows;
  }


  public function expandedType(): string {
    $result = $this->type;

    if ($this->type === 'entityreference') {
      $targetType = $this->fieldData['settings']['target_type'];
      $result .= "\n($targetType)";
    }

    if ($this->type == 'datetime') {
      $granularity = implode(",", array_keys(array_filter($this->fieldData['settings']['granularity'])));
      $result .= "\n($granularity)";
    }

    return $result;
  }


  public function expandedCardinality(): string {
    switch ($this->cardinality) {
      case 1:
        return '1';
      case -1:
        return 'many';
      default:
        return "multiple ({$this->cardinality})";
    }
  }


  public function usage(): string {
    $con = Database::getConnection('default', 'migrate');

    $fieldDataTable = "field_data_{$this->fieldName}";

    $query = $con->select($fieldDataTable, 'f');
    $query->addExpression('COUNT(*)', 'number_of_values');
    $query->addExpression('COUNT(DISTINCT f.entity_id)', 'number_of_entities');
    $query->condition('f.entity_type', $this->entityType);
    $query->condition('f.bundle', $this->bundle);
    $result = $query->execute()->fetchObject();

    $numberOfValues = $result->number_of_values;
    $numberOfEntities = $result->number_of_entities;

    $prefix = \Drupal::config('d7_field_analysis_google_sheets.settings')->get('d7_field_analysis_prefix');
    $prefix = $prefix ?: \Drupal::request()->getSchemeAndHttpHost();
    $text = ($numberOfValues > $numberOfEntities)
      ? "{$numberOfValues} ({$numberOfEntities})"
      : $numberOfValues;
    $link = "{$prefix}/d7-field-analysis/{$this->entityType}/{$this->bundle}/{$this->fieldName}";
    return "=HYPERLINK(\"$link\", \"$text\")";
  }


  public function references(): string {
    $con = Database::getConnection('default', 'migrate');
    $lines = [];

    if ($this->type === 'entityreference') {
      $handler = $this->fieldData['settings']['handler'];
      if ($handler === 'views') {
        $view = $this->fieldData['settings']['handler_settings']['view']['view_name'];
        $display = $this->fieldData['settings']['handler_settings']['view']['display_name'];
        $lines[] = "View: $view";
        $lines[] = "Display: $display";
        $lines[] = '';
      }
    }

    $referencesWithUsage = [];
    if ($this->type === 'taxonomy_term_reference') {
      $referencesWithUsage = $this->calculateTaxonomyTermReferencesWithUsage($con);
    }
    elseif ($this->type === 'entityreference') {
      $referencesWithUsage = $this->calculateEntityReferencesWithUsage($con);
    }
    elseif ($this->type === 'paragraphs') {
      $referencesWithUsage = $this->calculateParagraphReferencesWithUsage($con);
    }

    foreach ($referencesWithUsage as $bundle => $quantity) {
      $lines[] = "$bundle ($quantity)";
    }
    return implode("\n", $lines);
  }


  function calculateEntityReferencesWithUsage(Connection $con) {
    $targetType = $this->fieldData['settings']['target_type'];
    $targetTypeInfo = Manager::getInstance()->getEntityTypeInfo($targetType);
    $handler = $this->fieldData['settings']['handler'];

    // Make sure that all allowed bundles are listed even if there is zero usage.
    $allowedBundles = ($handler === 'base')
      ? array_keys($this->fieldData['settings']['handler_settings']['target_bundles'])
      : [];
    $usage = array_fill_keys($allowedBundles, 0);

    if ($targetTypeInfo instanceof SupportsEntityReferenceField) {
      $actualUsage = $targetTypeInfo->loadPerBundleUsageForEntityReferenceField($con, $this);
      $usage = array_merge($usage, $actualUsage);
    }

    return $usage;
  }


  // TODO: move this to VocabularyInfo::loadPerBundleUsageForTaxonomyTermReferenceField

  function calculateTaxonomyTermReferencesWithUsage(Connection $con) {
    $results = [];

    // Make sure that all allowed vocabularies are listed,
    // even if there is zero usage.
    foreach ($this->fieldData['settings']['allowed_values'] as $allowedValue) {
      $results[$allowedValue['vocabulary']] = 0;
    }

    // The SQL query looks something like this:
    // SELECT v.machine_name, COUNT(t.tid)
    // FROM field_data_somefield f
    // INNER JOIN taxonomy_term_data t ON t.tid=f.somefield_tid
    // INNER JOIN taxonomy_vocabulary v ON v.vid=t.vid
    // WHERE f.entity_type='node' AND f.bundle='somebundle'
    // GROUP BY v.machine_name

    $fieldDataTable = "field_data_{$this->fieldName}";
    $fieldDataTableTargetColumn = "{$this->fieldName}_tid";

    $query = $con->select($fieldDataTable, 'f');
    $query->addField('v', 'machine_name');
    $query->addExpression("count(t.tid)", 'quantity');
    $query->innerJoin('taxonomy_term_data', 't', "t.tid=f.{$fieldDataTableTargetColumn}");
    $query->innerJoin('taxonomy_vocabulary', 'v', 'v.vid=t.vid');
    $query->condition('f.entity_type', $this->entityType);
    $query->condition('f.bundle', $this->bundle);
    $query->groupBy("v.machine_name");

    foreach ($query->execute()->fetchAllKeyed() as $vocabulary => $quantity) {
      $results[$vocabulary] = $quantity;
    }

    return $results;
  }

  // TODO: move this to ParagraphInfo::loadPerBundleUsageForParagraphField

  function calculateParagraphReferencesWithUsage(Connection $con) {
    $results = [];

    // Make sure that all allowed paragraph types are listed,
    // even if they have zero occurrences.
    foreach ($this->instanceData['settings']['allowed_bundles'] as $bundle => $value) {
      if ($value != -1) {
        $results[$bundle] = 0;
      }
    }

    $fieldDataTable = "field_data_{$this->fieldName}";
    $fieldDataValueColumn = "{$this->fieldName}_value";
    $fieldDataRevisionColumn = "{$this->fieldName}_revision_id";

    $query = $con->select($fieldDataTable, 'f');
    $query->addField('p', 'bundle');
    $query->addExpression('count(*)', 'quantity');
    $query->innerJoin('paragraphs_item', 'p', "p.item_id=f.{$fieldDataValueColumn} AND p.revision_id=f.{$fieldDataRevisionColumn}");
    $query->condition('f.entity_type', $this->entityType);
    $query->condition('f.bundle', $this->bundle);
    $query->groupBy('p.bundle');

    foreach ($query->execute()->fetchAllKeyed() as $bundle => $quantity) {
      $results[$bundle] = $quantity;
    }

    return $results;
  }


  // Populate $this->childParagraphs
  protected function loadChildParagraphs(Connection $con): void {
    if ($this->type === 'paragraphs') {

      // Make sure that all allowed paragraph types are listed,
      // even if they have zero occurrences.
      foreach ($this->instanceData['settings']['allowed_bundles'] as $bundle => $value) {
        if ($value != -1) {
          $this->childParagraphs[$bundle] = 0;
        }
      }

      $fieldDataTable = "field_data_{$this->fieldName}";
      $fieldDataValueColumn = "{$this->fieldName}_value";
      $fieldDataRevisionColumn = "{$this->fieldName}_revision_id";

      $query = $con->select($fieldDataTable, 'f');
      $query->addField('p', 'bundle');
      $query->addExpression('count(*)', 'quantity');
      $query->innerJoin('paragraphs_item', 'p', "p.item_id=f.{$fieldDataValueColumn} AND p.revision_id=f.{$fieldDataRevisionColumn}");
      $query->condition('f.entity_type', $this->entityType);
      $query->condition('f.bundle', $this->bundle);
      $query->groupBy('p.bundle');

      foreach ($query->execute()->fetchAllKeyed() as $bundle => $quantity) {
        $this->childParagraphs[$bundle] = $quantity;
      }

    }
  }



  function __toString() {
    return "{$this->entityType}.{$this->bundle}.{$this->fieldName}";
  }

}
