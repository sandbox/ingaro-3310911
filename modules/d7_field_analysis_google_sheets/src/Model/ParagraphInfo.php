<?php

namespace Drupal\d7_field_analysis_google_sheets\Model;

use Drupal\Core\Database\Connection;

class ParagraphInfo extends EntityTypeInfo implements ChildEntityInfoInterface {

  use ChildEntityInfoTrait;


  /**
   * {@inheritdoc}
   */
  public function loadBundles(Connection $con) {
    $sql = 'select b.bundle, b.name, (select count(item_id) from {paragraphs_item} p where p.bundle=b.bundle) as qty from {paragraphs_bundle} b order by b.name';
    $results = $con->query($sql)
      ->fetchAll();

    foreach ($results as $result) {
      $bundle = new BundleInfo($con, $this->getEntityTypeId(), $result->bundle);
      $bundle->label = $result->name;
      $bundle->quantity = $result->qty;
      $this->bundles[$result->bundle] = $bundle;
    }
  }

  public function getEntityTypeId(): string {
    return 'paragraphs_item';
  }

  public function getLabel(): string {
    return 'Paragraph';
  }

  public function entityTable(): string {
    return 'paragraphs_item';
  }

  public function entityTableIdColumn(): string {
    return 'item_id';
  }

  public function entityTableBundleColumn(): string {
    return 'bundle';
  }


  function loadUsage(Connection $con) {
    $query = $con->select('field_config_instance', 'fi');
    $query->addField('fi', 'entity_type');
    $query->addField('fi', 'bundle');
    $query->addField('f', 'field_name');
    $query->addField('fi', 'data', 'instance_data');
    $query->innerJoin('field_config', 'f', 'f.id=fi.field_id');
    $query->condition('f.type', 'paragraphs');
    $query->orderBy('fi.entity_type');
    $query->orderBy('fi.bundle');
    $query->orderBy('f.field_name');
    $rows = $query->execute()->fetchAll();

    foreach ($rows as $row) {
      $identifier = "{$row->entity_type}.{$row->bundle}.{$row->field_name}";

      $data = unserialize($row->instance_data);
      $allowedBundles = $data['settings']['allowed_bundles'];
      $allowedBundles = array_filter(array_values($allowedBundles), fn($bundle) => $bundle != -1);
      foreach ($allowedBundles as $bundle) {
        $this->inboundUsage[$bundle][$identifier] = 0;
      }

    }
  }


}
