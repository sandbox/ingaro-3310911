<?php

namespace Drupal\d7_field_analysis_google_sheets\Model;

// For use with ChildEntityInfoInterface

trait ChildEntityInfoTrait {

  /**
   * Key = string (bundleId)
   * Value = array, whose keys are identifiers to fields, of the form entityType.bundle.fieldName
   *                and values are the number of such field values
   */
  protected array $inboundUsage = [];

  public function getInboundUsage(string $bundleId): array {
    return $this->inboundUsage[$bundleId] ?? [];
  }

}
