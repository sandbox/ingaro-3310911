<?php

namespace Drupal\d7_field_analysis_google_sheets;

use Drupal\d7_field_analysis_google_sheets\Model\ChildEntityInfoInterface;
use Drupal\d7_field_analysis_google_sheets\Model\EntityTypeInfo;
use Drupal\d7_field_analysis_google_sheets\Sheet\SheetMapping;
use Google\Service\Sheets\GridRange;
use Google\Service\Sheets\ValueRange;

class PopulateOverviewAppearsInColumn implements Task {

  use DefaultTaskTrait;


  protected EntityTypeInfo $entityType;

  public function __construct(EntityTypeInfo $entityType) {
    $this->entityType = $entityType;
  }


  public function build(SheetMapping $sheetMapping, array &$requests, array &$valueRanges): void {
    if ($this->entityType instanceof ChildEntityInfoInterface) {
      $headerRowRange = $sheetMapping->getNamedRange('overview.header_row')->getRange();
      $reservedColumnsRange = $sheetMapping->getNamedRange('overview.reserved_columns')->getRange();
      $columnIndex = $reservedColumnsRange->endColumnIndex - 1;

      $range = new GridRange([
        'sheetId' => 0,
        'startRowIndex' => $headerRowRange->endRowIndex,
        'startColumnIndex' => $columnIndex,
        'endColumnIndex' => $columnIndex + 1,
      ]);

      $values = [];

      foreach ($this->entityType->getBundles() as $bundleId => $bundle) {
        $inboundUsage = $this->entityType->getInboundUsage($bundleId);
        $values[] = [$this->formatAggregatedUsage($inboundUsage)];
      }

      // Header text
      $valueRanges[] = new ValueRange([
        'range' => $sheetMapping->a1Notation($range),
        'values' => $values,
      ]);

    }
  }


  protected function formatAggregatedUsage(array $inboundUsage): string {
    $lines = [];

    foreach ($inboundUsage as $source => $quantity) {
      $lines[] = "$source ($quantity)";
    }

    return implode("\n", $lines);
  }


}
