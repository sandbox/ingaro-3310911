<?php

namespace Drupal\d7_field_analysis_google_sheets;

use Drupal\d7_field_analysis_google_sheets\Sheet\SheetMapping;

trait DefaultTaskTrait {

  public function defineSheets(): array {
    return [];
  }

  public function defineNamedRanges(SheetMapping $sheetMapping): array {
    return [];
  }

  public function defineProtectedRanges(): array {
    return [];
  }

  public function checkPrerequisites(SheetMapping $sheetMapping) {}

  public function build(SheetMapping $sheetMapping, array &$requests, array &$valueRanges): void {}

}
