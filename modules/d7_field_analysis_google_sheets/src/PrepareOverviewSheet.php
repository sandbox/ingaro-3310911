<?php

namespace Drupal\d7_field_analysis_google_sheets;

use Drupal\d7_field_analysis_google_sheets\Model\ChildEntityInfoInterface;
use Drupal\d7_field_analysis_google_sheets\Model\EntityTypeInfo;
use Drupal\d7_field_analysis_google_sheets\Sheet\Helper;
use Drupal\d7_field_analysis_google_sheets\Sheet\SheetMapping;
use Google\Service\Sheets\GridRange;
use Google\Service\Sheets\Request;
use Google\Service\Sheets\ValueRange;

class PrepareOverviewSheet implements Task {

  const SHEET_ID = 0;


  protected bool $hasAppearsInColumn;

  public function __construct(EntityTypeInfo $entityType) {
    $this->hasAppearsInColumn = ($entityType instanceof ChildEntityInfoInterface);
  }


  public function defineSheets(): array {
    // We don't define an overview sheet, because we always use sheet 0 and assume it already exists.
    return [];
  }


  // TODO: go in an interface
  public function defineNamedRanges(SheetMapping $sheetMapping): array {
    return [
      'overview.reserved_columns' => [
        'sheetId' => 0,
        'startRowIndex' => 0,
        'startColumnIndex' => 0,
        'endColumnIndex' => $this->hasAppearsInColumn ? 4 : 3,
      ],
      'overview.header_row' => [
        'sheetId' => 0,
        'startRowIndex' => 0,
        'endRowIndex' => 1,
      ],
    ];
  }


  // TODO: go in an interface
  public function defineProtectedRanges(): array {
    return [
      'overview.reserved_columns',
    ];
  }


  public function checkPrerequisites(SheetMapping $sheetMapping) {

    // Brand-new spreadsheets consist of a sole sheet (tab) with ID=0.
    // We use this sheet for the overview.
    // In the unlikely event that there is no such sheet, fail. This scenario
    // can only occur if the initial sheet in a spreadsheet was deleted.
    if (!$sheetMapping->hasOverviewSheet()) {
      // Fail if there is no sheet with ID=0.
      throw new \RuntimeException("The given spreadsheet does not contain a sheet with ID=0. Try another spreadsheet.");
    }
  }


  public function build(SheetMapping $sheetMapping, array &$requests, array &$valueRanges): void {
    $headerRowRange = $sheetMapping->getNamedRange('overview.header_row')->getRange();
    $reservedColumnsRange = $sheetMapping->getNamedRange('overview.reserved_columns')->getRange();
    $columnOffset = $reservedColumnsRange->endColumnIndex;

    $requests[] = new Request([
      'updateSheetProperties' => [
        'properties' => [
          'sheetId' => self::SHEET_ID,
          'title' => 'Overview',
        ],
        'fields' => 'title',
      ],
    ]);

    $requests[] = Helper::makeRequestToSetVerticalAlignmentForSheet(self::SHEET_ID, Helper::TOP);

    $columnIndex = $columnOffset + 0;
    $requests[] = Helper::makeRequestToResizeColumn(self::SHEET_ID, $columnIndex, 300);
    $columnIndex = $columnOffset + 1;
    $requests[] = Helper::makeRequestToResizeColumn(self::SHEET_ID, $columnIndex, 300);
    $columnIndex = $columnOffset + 2;
    $requests[] = Helper::makeRequestToResizeColumn(self::SHEET_ID, $columnIndex, 80);
    $requests[] = Helper::makeRequestToSetHorizontalAlignmentForColumn(self::SHEET_ID, $columnIndex, Helper::RIGHT);

    if ($this->hasAppearsInColumn) {
      $columnIndex = $columnOffset + 3;
      $requests[] = Helper::makeRequestToResizeColumn(self::SHEET_ID, $columnIndex, 300);
    }

    $requests[] = Helper::makeRequestToFormatRangeAsHeader($headerRowRange);
    $requests[] = Helper::makeRequestToSetRowBorder(self::SHEET_ID, $headerRowRange->startColumnIndex, 'bottom');

    $range = new GridRange([
      'sheetId' => 0,
      'startRowIndex' => $headerRowRange->startRowIndex,
      'endRowIndex' => $headerRowRange->endRowIndex,
      'startColumnIndex' => $reservedColumnsRange->startColumnIndex,
      'endColumnIndex' => $reservedColumnsRange->endColumnIndex,
    ]);

    $headings = ['Label', 'Machine name', 'Quantity'];
    if ($this->hasAppearsInColumn) {
      $headings[] = 'Appears within';
    }

    // Header text
    $valueRanges[] = new ValueRange([
      'range' => $sheetMapping->a1Notation($range),
      'values' => [$headings],
    ]);
  }


}
