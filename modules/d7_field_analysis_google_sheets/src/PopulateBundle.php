<?php

namespace Drupal\d7_field_analysis_google_sheets;

use Drupal\d7_field_analysis_google_sheets\Sheet\Helper;
use Drupal\d7_field_analysis_google_sheets\Sheet\SheetMapping;
use Google\Service\Sheets\ValueRange;

class PopulateBundle extends BundleTask {

  use DefaultTaskTrait;



  public function checkPrerequisites(SheetMapping $sheetMapping) {
    if (!$sheetMapping->hasSheetWithTitle($this->sheetTitle)) {
      throw new \RuntimeException("Could not find sheet called {$this->sheetTitle}");
    }
    if (!$sheetMapping->hasNamedRange("{$this->sheetTitle}.reserved_columns")) {
      throw new \RuntimeException("Could not find named range {$this->sheetTitle}.reserved_columns");
    }
    if (!$sheetMapping->hasNamedRange("{$this->sheetTitle}.column_headings_row")) {
      throw new \RuntimeException("Could not find named range {$this->sheetTitle}.column_headings_row");
    }
  }


  public function build(SheetMapping $sheetMapping, array &$requests, array &$valueRanges): void {
    $sheetId = $sheetMapping->getSheetId($this->sheetTitle);
    $rowOffset = $sheetMapping->getNamedRange("{$this->sheetTitle}.column_headings_row")->getRange()->startRowIndex + 1;

    foreach ($this->bundle->fieldInstances as $field) {
      $values = $field->toGoogleSheetValues();
      $size = count($values);

      $firstRowNumber = $rowOffset + 1;
      $lastRowNumber = $rowOffset + $size;
      $range = "{$this->sheetTitle}!A{$firstRowNumber}:H{$lastRowNumber}";
      $valueRanges[] = new ValueRange([
        'range' => $range,
        'values' => $values,
      ]);

      if ($size > 1) {
        $startRowIndex = $rowOffset;
        $endRowIndex = $rowOffset + $size;
      }

      $rowOffset += count($values);
    }
  }



}
