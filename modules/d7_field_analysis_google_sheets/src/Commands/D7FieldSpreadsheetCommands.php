<?php

namespace Drupal\d7_field_analysis_google_sheets\Commands;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Database\Database;
use Drupal\d7_field_analysis_google_sheets\Manager;
use Drupal\d7_field_analysis_google_sheets\Model\ChildEntityInfoInterface;
use Drupal\d7_field_analysis_google_sheets\PopulateBundle;
use Drupal\d7_field_analysis_google_sheets\PopulateOverview;
use Drupal\d7_field_analysis_google_sheets\PopulateOverviewAppearsInColumn;
use Drupal\d7_field_analysis_google_sheets\PrepareBundleNotesColumns;
use Drupal\d7_field_analysis_google_sheets\PrepareBundleSourceColumns;
use Drupal\d7_field_analysis_google_sheets\PrepareBundleSheet;
use Drupal\d7_field_analysis_google_sheets\PrepareBundleTargetColumns;
use Drupal\d7_field_analysis_google_sheets\PrepareOverviewNotesColumns;
use Drupal\d7_field_analysis_google_sheets\PrepareOverviewSheet;
use Drupal\d7_field_analysis_google_sheets\Sheet\Writer;
use Drush\Commands\DrushCommands;

class D7FieldSpreadsheetCommands extends DrushCommands {

  /**
   * Populate a Google sheet with details of bundles and fields.
   *
   * @command d7-field-analysis:google-sheet
   * @param $entityTypeId string An entity type ID, eg node, taxonomy_term etc.
   * @param $spreadsheetId string The Google sheet ID (obtainable from its URL)
   */
  public function node(string $entityTypeId, string $spreadsheetId) {
    $entityTypeInfo = Manager::getInstance()->getEntityTypeInfo($entityTypeId);

    if ($entityTypeInfo instanceof ChildEntityInfoInterface) {
      $entityTypeInfo->loadUsage(Database::getConnection('default', 'migrate'));
    }

    // TODO: can we do anything better than this hardcoded path?
    $credentials = Json::decode(file_get_contents('private://d7_field_analysis_google_sheets.credentials.json'));

    $tasks = [];
    $tasks[] = new PrepareOverviewSheet($entityTypeInfo);
    $tasks[] = new PrepareOverviewNotesColumns();

    foreach ($entityTypeInfo->getBundles() as $id => $bundle) {
      $tasks[] = new PrepareBundleSheet($id, $bundle);
      $tasks[] = new PrepareBundleSourceColumns($id, $bundle);
      $tasks[] = new PrepareBundleNotesColumns($id, $bundle);
      $tasks[] = new PrepareBundleTargetColumns($id, $bundle);
      $tasks[] = new PopulateBundle($id, $bundle);
    }

    $tasks[] = new PopulateOverview($entityTypeInfo);
    $tasks[] = new PopulateOverviewAppearsInColumn($entityTypeInfo);

    $writer = new Writer($credentials, $spreadsheetId, $this->logger());
    $writer->processTasks($tasks);
  }

}
