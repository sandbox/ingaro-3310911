<?php

namespace Drupal\d7_field_analysis_google_sheets;

use Drupal\d7_field_analysis_google_sheets\Sheet\SheetMapping;
use Google\Service\Sheets\Request;
use Google\Service\Sheets\ValueRange;

interface Task {

  public function defineSheets(): array;

  /**
   * @return array
   *   Key=name, value=array
   */
  public function defineNamedRanges(SheetMapping $sheetMapping): array;

  /**
   * A list of named range names that are also protected.
   *
   * @return string[]
   */
  public function defineProtectedRanges(): array;


  public function checkPrerequisites(SheetMapping $sheetMapping);

  /**
   * @param SheetMapping $sheetMapping
   * @param Request[] $requests
   * @param ValueRange[] $valueRanges
   */
  public function build(SheetMapping $sheetMapping, array &$requests, array &$valueRanges): void;

}
