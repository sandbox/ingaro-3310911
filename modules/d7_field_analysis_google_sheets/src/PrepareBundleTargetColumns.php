<?php

namespace Drupal\d7_field_analysis_google_sheets;

use Drupal\d7_field_analysis_google_sheets\Sheet\Helper;
use Drupal\d7_field_analysis_google_sheets\Sheet\SheetMapping;
use Google\Service\Sheets\GridRange;
use Google\Service\Sheets\Request;
use Google\Service\Sheets\ValueRange;

class PrepareBundleTargetColumns extends BundleTask {

  use DefaultTaskTrait;

  const COLUMN_OFFSET = PrepareBundleNotesColumns::COLUMN_OFFSET + PrepareBundleNotesColumns::NUMBER_OF_COLUMNS;
  const NUMBER_OF_COLUMNS = 5;


  public function defineNamedRanges(SheetMapping $sheetMapping): array {
    $sheetId = $sheetMapping->getSheetId($this->sheetTitle);

    return [
      "{$this->sheetTitle}.target_columns" => [
        'sheetId' => $sheetId,
        'startRowIndex' => 0,
        'startColumnIndex' => self::COLUMN_OFFSET,
        'endColumnIndex' => self::COLUMN_OFFSET + self::NUMBER_OF_COLUMNS,
      ],
    ];
  }


  public function checkPrerequisites(SheetMapping $sheetMapping) {
    if (!$sheetMapping->hasSheetWithTitle($this->sheetTitle)) {
      throw new \RuntimeException("Could not find sheet called {$this->sheetTitle}");
    }
    if (!$sheetMapping->hasNamedRange("{$this->sheetTitle}.notes_columns")) {
      throw new \RuntimeException("Could not find named range {$this->sheetTitle}.notes_columns");
    }
  }

  public function build(SheetMapping $sheetMapping, array &$requests, array &$valueRanges): void {
    $sheetId = $sheetMapping->getSheetId($this->sheetTitle);
    $rowOffset = $sheetMapping->getNamedRange("{$this->sheetTitle}.column_headings_row")->getRange()->startRowIndex;
    $columnOffset = $sheetMapping->getNamedRange("{$this->sheetTitle}.target_columns")->getRange()->startColumnIndex;

    // Set up column widths & alignment.
    $requests[] = Helper::makeRequestToResizeColumn($sheetId, $columnOffset + 0, 250); // label
    $requests[] = Helper::makeRequestToResizeColumn($sheetId, $columnOffset + 1, 200); // machine name
    $requests[] = Helper::makeRequestToResizeColumn($sheetId, $columnOffset + 2, 200); // type
    $requests[] = Helper::makeRequestToResizeColumn($sheetId, $columnOffset + 3, 80);  // cardinality
    $requests[] = Helper::makeRequestToSetHorizontalAlignmentForColumn($sheetId, $columnOffset + 3, Helper::CENTER);
    $requests[] = Helper::makeRequestToResizeColumn($sheetId, $columnOffset + 4, 80);  // required
    $requests[] = Helper::makeRequestToSetHorizontalAlignmentForColumn($sheetId, $columnOffset + 4, Helper::CENTER);

    // Add a border to the left of this set of columns.
    $requests[] = new Request([
      'updateBorders' => [
        'range' => [
          'sheetId' => $sheetId,
          'startRowIndex' => $rowOffset,
          'startColumnIndex' => $columnOffset,
          'endColumnIndex' => $columnOffset + 1,
        ],
        'left' => [
          'style' => 'SOLID_THICK',
          'color' => ['red'=> 0.4, 'green' => 0.4, 'blue' => 0.4],
        ],
      ],
    ]);

    // Populate cells.
    $titles = ['Label', 'Machine name', 'Type', 'Cardinality', 'Required'];
    $valueRanges[] = new ValueRange([
      'range' => $sheetMapping->a1Notation(new GridRange([
        'sheetId' => $sheetId,
        'startRowIndex' => $rowOffset,
        'endRowIndex' => $rowOffset + 1,
        'startColumnIndex' => $columnOffset,
        'endColumnIndex' => $columnOffset + self::NUMBER_OF_COLUMNS,
      ])),
      'values' => [$titles],
    ]);
  }


}
