<?php

namespace Drupal\d7_field_analysis_google_sheets;

use Drupal\d7_field_analysis_google_sheets\Model\EntityTypeInfo;
use Drupal\d7_field_analysis_google_sheets\Sheet\SheetMapping;
use Google\Service\Sheets\ValueRange;
use RuntimeException;

class PopulateOverview implements Task {

  use DefaultTaskTrait;


  protected EntityTypeInfo $entityTypeInfo;

  public function __construct(EntityTypeInfo $entityTypeInfo) {
    $this->entityTypeInfo = $entityTypeInfo;
  }


  public function checkPrerequisites(SheetMapping $sheetMapping) {
    if (!$sheetMapping->hasOverviewSheet()) {
      throw new RuntimeException('No overview sheet');
    }
    if (!$sheetMapping->hasNamedRange('overview.reserved_columns') || !$sheetMapping->hasNamedRange('overview.header_row')) {
      throw new RuntimeException('The required named ranges do not exist');
    }
  }

  public function build(SheetMapping $sheetMapping, array &$requests, array &$valueRanges): void {
    $values = [];
    foreach ($this->entityTypeInfo->getBundles() as $bundleId => $bundle) {
      $label = $sheetMapping->hasBundleSheet($bundleId)
        ? $sheetMapping->getSheetHyperlink($sheetMapping->getBundleSheetId($bundleId), $bundle->label)
        : $bundle->label;
      $values[] = [$label, $bundle->name, $bundle->quantity];
    }

    // TODO: some function to shorten this
    $rowNumber = $sheetMapping->getNamedRange('overview.header_row')->getRange()->getStartRowIndex() + 2;
    $range = "Overview!A{$rowNumber}:C";

    $valueRanges[] = new ValueRange([
      'range' => $range,
      'values' => $values,
    ]);
  }

}
