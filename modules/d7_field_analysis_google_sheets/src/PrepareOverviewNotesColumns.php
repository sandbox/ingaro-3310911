<?php

namespace Drupal\d7_field_analysis_google_sheets;

use Drupal\d7_field_analysis_google_sheets\Sheet\Helper;
use Drupal\d7_field_analysis_google_sheets\Sheet\SheetMapping;
use Google\Service\Sheets\GridRange;
use Google\Service\Sheets\Request;
use Google\Service\Sheets\ValueRange;

class PrepareOverviewNotesColumns implements Task {

  use DefaultTaskTrait;


  public function build(SheetMapping $sheetMapping, array &$requests, array &$valueRanges): void {
    $sheetId = 0;
    $rowOffset = $sheetMapping->getNamedRange("overview.header_row")->getRange()->startRowIndex;
    $columnOffset = $sheetMapping->getNamedRange("overview.reserved_columns")->getRange()->endColumnIndex;

    // Migrate? column
    $columnIndex = $columnOffset + 0;
    $requests[] = Helper::makeRequestToResizeColumn($sheetId, $columnIndex, 80);
    $requests[] = Helper::makeRequestToSetHorizontalAlignmentForColumn($sheetId, $columnIndex, Helper::CENTER);

    // Migrate done? column
    $columnIndex = $columnOffset + 1;
    $requests[] = Helper::makeRequestToResizeColumn($sheetId, $columnIndex, 80);
    $requests[] = Helper::makeRequestToSetHorizontalAlignmentForColumn($sheetId, $columnIndex, Helper::CENTER);

    // Destination column
    $columnIndex = $columnOffset + 2;
    $requests[] = Helper::makeRequestToResizeColumn($sheetId, $columnIndex, 160);
    $requests[] = Helper::makeRequestToSetHorizontalAlignmentForColumn($sheetId, $columnIndex, Helper::LEFT);

    // Migration column
    $columnIndex = $columnOffset + 3;
    $requests[] = Helper::makeRequestToResizeColumn($sheetId, $columnIndex, 160);
    $requests[] = Helper::makeRequestToSetHorizontalAlignmentForColumn($sheetId, $columnIndex, Helper::LEFT);

    // Annertech notes column
    $columnIndex = $columnOffset + 4;
    $requests[] = Helper::makeRequestToResizeColumn($sheetId, $columnIndex, 500);
    $requests[] = Helper::makeRequestToSetHorizontalAlignmentForColumn($sheetId, $columnIndex, Helper::LEFT);
    $requests[] = Helper::makeRequestToSetWrapStrategyForColumn($sheetId, $columnIndex, 'WRAP');

    // Client notes column
    $columnIndex = $columnOffset + 5;
    $requests[] = Helper::makeRequestToResizeColumn($sheetId, $columnIndex, 500);
    $requests[] = Helper::makeRequestToSetHorizontalAlignmentForColumn($sheetId, $columnIndex, Helper::LEFT);
    $requests[] = Helper::makeRequestToSetWrapStrategyForColumn($sheetId, $columnIndex, 'WRAP');

    // Add a border to the left of this set of columns.
    $requests[] = new Request([
      'updateBorders' => [
        'range' => [
          'sheetId' => $sheetId,
          'startRowIndex' => $rowOffset,
          'startColumnIndex' => $columnOffset,
          'endColumnIndex' => $columnOffset + 1,
        ],
        'left' => [
          'style' => 'SOLID_THICK',
          'color' => ['red'=> 0.4, 'green' => 0.4, 'blue' => 0.4],
        ],
      ],
    ]);

    // Populate cells.
    $titles = ['Migrate?', 'Done?', 'Destination', 'Migration', 'Annertech notes', 'Client notes'];
    $valueRanges[] = new ValueRange([
      'range' => $sheetMapping->a1Notation(new GridRange([
        'sheetId' => $sheetId,
        'startRowIndex' => $rowOffset,
        'endRowIndex' => $rowOffset + 1,
        'startColumnIndex' => $columnOffset,
        'endColumnIndex' => $columnOffset + 6,
      ])),
      'values' => [$titles],
    ]);
  }

}
