<?php

namespace Drupal\d7_field_analysis_google_sheets;

use Drupal\d7_field_analysis_google_sheets\Model\FieldInstanceInfo;
use Drupal\d7_field_analysis_google_sheets\Sheet\Helper;
use Drupal\d7_field_analysis_google_sheets\Sheet\SheetMapping;
use Google\Service\Sheets\GridRange;
use Google\Service\Sheets\ValueRange;

class PrepareBundleSourceColumns extends BundleTask {

  use DefaultTaskTrait;

  const NUMBER_OF_COLUMNS = 8;


  public function defineNamedRanges(SheetMapping $sheetMapping): array {
    $sheetId = $sheetMapping->getSheetId($this->sheetTitle);

    return [
      "{$this->sheetTitle}.reserved_columns" => [
        'sheetId' => $sheetId,
        'startRowIndex' => 0,
        'startColumnIndex' => 0,
        'endColumnIndex' => self::NUMBER_OF_COLUMNS,
      ],
    ];
  }

  public function defineProtectedRanges(): array {
    return [
      "{$this->sheetTitle}.reserved_columns",
    ];
  }



  public function checkPrerequisites(SheetMapping $sheetMapping) {
    if (!$sheetMapping->hasSheetWithTitle($this->sheetTitle)) {
      throw new \RuntimeException("Could not find sheet called {$this->sheetTitle}");
    }
  }


  public function build(SheetMapping $sheetMapping, array &$requests, array &$valueRanges): void {
    $sheetId = $sheetMapping->getSheetId($this->sheetTitle);

    $columnHeadingsRowRange = $sheetMapping->getNamedRange("{$this->sheetTitle}.column_headings_row")->getRange();
    $reservedColumnsRange = $sheetMapping->getNamedRange("{$this->sheetTitle}.reserved_columns")->getRange();
    $range = new GridRange([
      'sheetId' => $reservedColumnsRange->sheetId,
      'startRowIndex' => $columnHeadingsRowRange->startRowIndex,
      'endRowIndex' => $columnHeadingsRowRange->endRowIndex,
      'startColumnIndex' => $reservedColumnsRange->startColumnIndex,
      'endColumnIndex' => $reservedColumnsRange->endColumnIndex,
    ]);

    // Set up column widths & alignment.
    $requests[] = Helper::makeRequestToResizeColumn($sheetId, 0, 250);  // label
    $requests[] = Helper::makeRequestToResizeColumn($sheetId, 1, 200);  // machine name
    $requests[] = Helper::makeRequestToResizeColumn($sheetId, 2, 100);  // type
    $requests[] = Helper::makeRequestToResizeColumn($sheetId, 3, 100);  // property
    $requests[] = Helper::makeRequestToResizeColumn($sheetId, 4, 270);  // references
    $requests[] = Helper::makeRequestToResizeColumn($sheetId, 5, 80);   // usage
    $requests[] = Helper::makeRequestToSetHorizontalAlignmentForColumn($sheetId, 5, Helper::CENTER);
    $requests[] = Helper::makeRequestToResizeColumn($sheetId, 6, 80);   // cardinality
    $requests[] = Helper::makeRequestToSetHorizontalAlignmentForColumn($sheetId, 6, Helper::CENTER);
    $requests[] = Helper::makeRequestToResizeColumn($sheetId, 7, 80);   // required
    $requests[] = Helper::makeRequestToSetHorizontalAlignmentForColumn($sheetId, 7, Helper::CENTER);

    // Populate cells.
    $titles = FieldInstanceInfo::headerOutput();
    $valueRanges[] = new ValueRange([
      'range' => $sheetMapping->a1Notation($range),
      'values' => [$titles],
    ]);
  }


}
