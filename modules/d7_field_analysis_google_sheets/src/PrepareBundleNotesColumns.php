<?php

namespace Drupal\d7_field_analysis_google_sheets;

use Drupal\d7_field_analysis_google_sheets\Sheet\Helper;
use Drupal\d7_field_analysis_google_sheets\Sheet\SheetMapping;
use Google\Service\Sheets\GridRange;
use Google\Service\Sheets\Request;
use Google\Service\Sheets\ValueRange;

class PrepareBundleNotesColumns extends BundleTask {

  use DefaultTaskTrait;

  const COLUMN_OFFSET = PrepareBundleSourceColumns::NUMBER_OF_COLUMNS;
  const NUMBER_OF_COLUMNS = 5;


  public function defineNamedRanges(SheetMapping $sheetMapping): array {
    $sheetId = $sheetMapping->getSheetId($this->sheetTitle);

    return [
      "{$this->sheetTitle}.notes_columns" => [
        'sheetId' => $sheetId,
        'startRowIndex' => 0,
        'startColumnIndex' => self::COLUMN_OFFSET,
        'endColumnIndex' => self::COLUMN_OFFSET + self::NUMBER_OF_COLUMNS,
      ],
    ];
  }


  public function checkPrerequisites(SheetMapping $sheetMapping) {
    if (!$sheetMapping->hasSheetWithTitle($this->sheetTitle)) {
      throw new \RuntimeException("Could not find sheet called {$this->sheetTitle}");
    }
    if (!$sheetMapping->hasNamedRange("{$this->sheetTitle}.reserved_columns")) {
      throw new \RuntimeException("Could not find named range {$this->sheetTitle}.reserved_columns");
    }
  }

  public function build(SheetMapping $sheetMapping, array &$requests, array &$valueRanges): void {
    $sheetId = $sheetMapping->getSheetId($this->sheetTitle);
    $rowOffset = $sheetMapping->getNamedRange("{$this->sheetTitle}.column_headings_row")->getRange()->startRowIndex;
    $columnOffset = $sheetMapping->getNamedRange("{$this->sheetTitle}.notes_columns")->getRange()->startColumnIndex;

    // Migrate? column
    $columnIndex = $columnOffset + 0;
    $requests[] = Helper::makeRequestToResizeColumn($sheetId, $columnIndex, 80);
    $requests[] = Helper::makeRequestToSetHorizontalAlignmentForColumn($sheetId, $columnIndex, Helper::CENTER);

    // Migrate done? column
    $columnIndex = $columnOffset + 1;
    $requests[] = Helper::makeRequestToResizeColumn($sheetId, $columnIndex, 80);
    $requests[] = Helper::makeRequestToSetHorizontalAlignmentForColumn($sheetId, $columnIndex, Helper::CENTER);

    // Destination column
    $columnIndex = $columnOffset + 2;
    $requests[] = Helper::makeRequestToResizeColumn($sheetId, $columnIndex, 160);
    $requests[] = Helper::makeRequestToSetHorizontalAlignmentForColumn($sheetId, $columnIndex, Helper::LEFT);

    // Annertech notes column
    $columnIndex = $columnOffset + 3;
    $requests[] = Helper::makeRequestToResizeColumn($sheetId, $columnIndex, 300);
    $requests[] = Helper::makeRequestToSetHorizontalAlignmentForColumn($sheetId, $columnIndex, Helper::LEFT);
    $requests[] = Helper::makeRequestToSetWrapStrategyForColumn($sheetId, $columnIndex, 'WRAP');

    // Client notes column
    $columnIndex = $columnOffset + 4;
    $requests[] = Helper::makeRequestToResizeColumn($sheetId, $columnIndex, 300);
    $requests[] = Helper::makeRequestToSetHorizontalAlignmentForColumn($sheetId, $columnIndex, Helper::LEFT);
    $requests[] = Helper::makeRequestToSetWrapStrategyForColumn($sheetId, $columnIndex, 'WRAP');

    // Add a border to the left of this set of columns.
    $requests[] = new Request([
      'updateBorders' => [
        'range' => [
          'sheetId' => $sheetId,
          'startRowIndex' => $rowOffset,
          'startColumnIndex' => $columnOffset,
          'endColumnIndex' => $columnOffset + 1,
        ],
        'left' => [
          'style' => 'SOLID_THICK',
          'color' => ['red'=> 0.4, 'green' => 0.4, 'blue' => 0.4],
        ],
      ],
    ]);

    // Populate cells.
    $titles = ['Migrate?', 'Done?', 'Destination', 'Annertech notes', 'Client notes'];
    $valueRanges[] = new ValueRange([
      'range' => $sheetMapping->a1Notation(new GridRange([
        'sheetId' => $sheetId,
        'startRowIndex' => $rowOffset,
        'endRowIndex' => $rowOffset + 1,
        'startColumnIndex' => $columnOffset,
        'endColumnIndex' => $columnOffset + self::NUMBER_OF_COLUMNS,
      ])),
      'values' => [$titles],
    ]);
  }


}
