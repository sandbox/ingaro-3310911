<?php

namespace Drupal\d7_field_analysis;

class Helper {

  public static function niceEntityType(string $entityType): string {
    switch ($entityType) {
      case 'node':
        return "Node";
      case 'paragraphs_item':
        return "Paragraph";
      case 'field_collection_item':
        return "Field collection";
      case 'taxonomy_term':
        return "Term";
      default:
        return $entityType;
    }
  }

  public static function niceEntityTypeAndId(string $entityType, int $entityId): string {
    $niceEntityType = self::niceEntityType($entityType);
    return "$niceEntityType $entityId";
  }

  public static function niceEntityTypeAndRevision(string $entityType, int $entityId, int $revisionId): string {
    $niceEntityType = self::niceEntityType($entityType);
    return "$niceEntityType $entityId (revision $revisionId)";
  }

  public static function niceEntityTypeAndBundle(string $entityType, string $bundle): string {
    $niceEntityType = self::niceEntityType($entityType);
    return "$niceEntityType: $bundle";
  }

  public static function niceEntityTypeAndBundleAndId(string $entityType, string $bundle, int $entityId): string {
    $niceEntityType = self::niceEntityType($entityType);
    return "$niceEntityType $entityId ($bundle)";
  }

}
