<?php

namespace Drupal\d7_field_analysis\Routing;

use Symfony\Component\Routing\Route;

/**
 * Provides routes for field usage pages per D7 entity type.
 */
class FieldUsageRoutes {

  /**
   * Returns an array of route objects.
   *
   * @return \Symfony\Component\Routing\Route[]
   *   An array of route objects.
   */
  public function routes() {
    $routes = [];

    // "Top level" entities can be thought of as those with their own page,
    // such as node, taxonomy term, user. These don't have a "parent" entity.
    foreach (['node', 'taxonomy_term'] as $entityType) {
      $path = "/d7-field-analysis/$entityType/{bundle}/{fieldName}";
      $defaults = [
        '_title_callback' => '\Drupal\d7_field_analysis\Controller\PrimaryFieldUsageController::title',
        '_controller' => '\Drupal\d7_field_analysis\Controller\PrimaryFieldUsageController::page',
        'entityType' => $entityType,
      ];
      $requirements = [
        '_permission' => 'use d7_field_analysis',
      ];
      $options = [
        'no_cache' => TRUE,
        '_admin_route' => TRUE,
      ];
      $routes["d7_field_analysis.{$entityType}"] = new Route($path, $defaults, $requirements, $options);
    }

    // "Child" entities can be throught of as those that belong to a parent,
    // such as paragraphs and field collections.
    foreach (['paragraphs_item', 'field_collection_item'] as $entityType) {
      $path = "/d7-field-analysis/$entityType/{bundle}/{fieldName}";
      $defaults = [
        '_title_callback' => '\Drupal\d7_field_analysis\Controller\SecondaryFieldUsageController::title',
        '_controller' => '\Drupal\d7_field_analysis\Controller\SecondaryFieldUsageController::page',
        'entityType' => $entityType,
      ];
      $requirements = [
        '_permission' => 'use d7_field_analysis',
      ];
      $options = [
        'no_cache' => TRUE,
        '_admin_route' => TRUE,
      ];
      $routes["d7_field_analysis.{$entityType}"] = new Route($path, $defaults, $requirements, $options);
    }

    return $routes;
  }

}
