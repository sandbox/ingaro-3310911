<?php

namespace Drupal\d7_field_analysis;

use Drupal\Core\Database\Connection;

class Ancestry {

  protected static function makeKey(string $entityType, string $bundle, int $entityId) {
    return "{$entityType}.{$bundle}.{$entityId}";
  }

  protected static function makeValue(string $entityType, string $bundle, int $entityId, string $fieldName) {
    return "{$entityType}.{$bundle}.{$entityId}.{$fieldName}";
  }

  protected static function keyFromValue(string $value) {
    $matches = [];
    if (preg_match('/^([a-z0-9_]+\.[a-z0-9_]+\.[0-9]+)\.[a-z0-9_]+$/', $value, $matches)) {
      return $matches[1];
    }
    else {
      throw new \RuntimeException("Malformed value: $value");
    }
  }




  /**
   * Example:
   * [
   *   "paragraphs_item.6519" => "paragraphs_item.6524.field_accordion_items"
   *   "paragraphs_item.6524" => "node.1737.field_accordions"
   * ]
   */
  protected array $parents = [];

  /**
   * An array of values identifying to an entity and its lineage.
   *
   * For example:
   *   "node.course.1737.field_course_apply_accordion.paragraphs_item.accordion_menu_section.6524.field_ams_item.paragraphs_item.accordion_menu_section_item.6519"
   * means:
   *   there is a 'course' node (1737)
   *   ...whose field_course_apply_accordion' field contains
   *   ...an 'accordion_menu_section' paragraph (6524)
   *   ...whose 'field_ams_item' field contains
   *   ...an 'accordion_menu_section_item' paragraph wtih (6519)
   *
   * These strings are designed to be grepped in order to filter entities, eg
   * to find all paragraphs within a certain entity type/bundle/field combination.
   *
   * @var string[]
   */
  protected array $lineages = [];


  public function load(Connection $con) {
    self::loadParagraphsWithParents($con);
    self::loadFieldCollectionsWithParents($con);
    self::buildLineages();
  }


  public function loadParagraphsWithParents(Connection $con): void {
    $query = $con->select('field_config_instance', 'fi');
    $query->addField('f', 'field_name');
    $query->innerJoin('field_config', 'f', 'f.id=fi.field_id');
    $query->condition('f.type', 'paragraphs');
    $fieldNames = $query->execute()->fetchCol();

    foreach ($fieldNames as $fieldName) {
      self::loadParagraphsWithParentsForField($con, $fieldName);
    }
  }


  protected function loadParagraphsWithParentsForField(Connection $con, string $fieldName): void {
    $fieldDataTable = "field_data_{$fieldName}";
    $targetIdColumn = "{$fieldName}_value";
    $targetRevisionColumn = "{$fieldName}_revision_id";

    $query = $con->select($fieldDataTable, 'f');
    $query->addField('f', $targetIdColumn, 'childId');
    $query->addField('f', $targetRevisionColumn, 'childRevision');
    $query->addField('p', 'bundle', 'childBundle');
    $query->addField('f', 'entity_type', 'parentType');
    $query->addField('f', 'bundle', 'parentBundle');
    $query->addField('f', 'entity_id', 'parentId');
    $query->addField('f', 'revision_id', 'parentRevision');
    $query->innerJoin('paragraphs_item', 'p', "p.item_id=f.{$targetIdColumn}");  // TODO: revision?

    foreach ($query->execute()->fetchAll() as $row) {
      $key = self::makeKey('paragraphs_item', $row->childBundle, $row->childId);
      $value = self::makeValue($row->parentType, $row->parentBundle, $row->parentId, $fieldName);
      $this->parents[$key] = $value;
    }
  }


  protected function loadFieldCollectionsWithParents(Connection $con): void {
    $query = $con->select('field_config_instance', 'fi');
    $query->addField('f', 'field_name');
    $query->innerJoin('field_config', 'f', 'f.id=fi.field_id');
    $query->condition('f.type', 'field_collection');
    $fieldNames = $query->execute()->fetchCol();

    foreach ($fieldNames as $fieldName) {
      self::loadFieldCollectionsWithParentsForField($con, $fieldName);
    }
  }


  protected function loadFieldCollectionsWithParentsForField(Connection $con, string $fieldName): void {
    $fieldDataTable = "field_data_{$fieldName}";
    $targetIdColumn = "{$fieldName}_value";
    $targetRevisionColumn = "{$fieldName}_revision_id";

    $query = $con->select($fieldDataTable, 'f');
    $query->addField('f', $targetIdColumn, 'childId');
    $query->addField('f', $targetRevisionColumn, 'childRevision');
    $query->addField('fc', 'field_name', 'childBundle');
    $query->addField('f', 'entity_type', 'parentType');
    $query->addField('f', 'bundle', 'parentBundle');
    $query->addField('f', 'entity_id', 'parentId');
    $query->addField('f', 'revision_id', 'parentRevision');
    $query->innerJoin('field_collection_item', 'fc', "fc.item_id=f.{$targetIdColumn}");  // TODO: revision?

    foreach ($query->execute()->fetchAll() as $row) {
      $key = self::makeKey('field_collection_item', $row->childBundle, $row->childId);
      $value = self::makeValue($row->parentType, $row->parentBundle, $row->parentId, $fieldName);
      $this->parents[$key] = $value;
    }
  }


  protected function buildLineages() {
    foreach (array_keys($this->parents) as $key) {
      $ancestors = [];
      $ancestors[] = $key;
      while ($key && ($parent = $this->parents[$key] ?? NULL)) {
        $ancestors[] = $parent;
        $key = self::keyFromValue($parent);
      }
      $lineage = implode('|', array_reverse($ancestors));
      $this->lineages[] = $lineage;
    }
    sort($this->lineages);
  }


  public function getLineagesOfBundle(string $entityType, string $bundle): array {
    $pattern = "/{$entityType}\\.{$bundle}\\.[0-9]+$/";
    return array_filter($this->lineages, fn($lineage) => preg_match($pattern, $lineage));
  }

  public function getLineageOfEntity(string $entityType, int $entityId): ?string {
    $dot = '\.';
    $bundle = '[0-9a-z_]+';
    $pattern = "/{$entityType}{$dot}{$bundle}{$dot}{$entityId}$/";
    $results = array_filter($this->lineages, fn($lineage) => preg_match($pattern, $lineage));
    return reset($results);
  }

  public static function topLevelEntityType(string $lineage): string {
    $dot = '\.';
    $machineName = '[0-9a-z_]+';
    $pattern = "/^({$machineName}){$dot}/";
    return self::pregMatchSingle($pattern, $lineage);
  }

  public static function topLevelBundle(string $lineage): string {
    $dot = '\.';
    $machineName = '[0-9a-z_]+';
    $id = '[0-9]+';
    $pattern = "/^{$machineName}{$dot}({$machineName}){$dot}{$id}/";
    return self::pregMatchSingle($pattern, $lineage);
  }

  public static function topLevelEntityId(string $lineage): string {
    $dot = '\.';
    $machineName = '[0-9a-z_]+';
    $id = '[0-9]+';
    $pattern = "/^{$machineName}{$dot}{$machineName}{$dot}({$id})/";
    return (int) self::pregMatchSingle($pattern, $lineage);
  }


  public static function pregMatchSingle(string $pattern, string $subject): string {
    $matches = [];
    if (preg_match($pattern, $subject, $matches)) {
      return $matches[1];
    }
    else {
      throw new \InvalidArgumentException($subject);
    }
  }


  public static function group(array $lineages): array {
    $groups = [];
    foreach ($lineages as $lineage) {
      list($head, $tail) = explode('|', $lineage);
      $groups[$head][] = $lineage;
    }
    return $groups;
  }


  public static function explain($lineage) {
    if (empty($lineage)) {
      return '';
    }

    $generations = explode('|', $lineage);

    $niceEntityTypes = [
      'paragraphs_item' => 'paragraph',
    ];

    $lines = [];
    foreach ($generations as $level => $value) {
      $parts = explode('.', $value);
      $entityType = $parts[0];
      $bundle = $parts[1];
      $entityId = $parts[2];
      $fieldName = count($parts) > 3 ? $parts[3] : '';

      $niceEntityType = $niceEntityTypes[$entityType] ?? $entityType;
      $indent = str_repeat('..', $level);
      $lines[] = ($level === 0)
        ? "There is a $niceEntityType of type $bundle ($entityId)"
        : "{$indent}a $niceEntityType of type $bundle ($entityId)";
      if ($fieldName) {
        $lines[] = "{$indent}whose $fieldName field contains";
      }
    }

    return $lines;
  }

}
