<?php

namespace Drupal\d7_field_analysis\Controller;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Database;
use Drupal\d7_field_analysis\Ancestor;
use Drupal\d7_field_analysis\Ancestry;
use Drupal\d7_field_analysis\Helper;

class SecondaryFieldUsageController extends FieldUsageControllerBase {


  // returns ['bundle' => ..., 'label' => ...]

  public function getEntityDetails(Connection $con, string $entityType, int $entityId): array {
    switch ($entityType) {
      case 'node':
        $query = $con->select('node', 'n')
          ->fields('n', ['type', 'title'])
          ->condition('n.nid', $entityId)
          ->execute();
        $rows = $query->fetchAll();
        $row = reset($rows);
        if ($row) {
          return [
            'bundle' => $row->type,
            'label' => $row->title,
          ];
        }
        else {
          return [
            'bundle' => 'unknown',
            'label' => 'unknown',
          ];
        }

      case 'paragraphs_item':
        $query = $con->select('paragraphs_item', 'p')
          ->fields('p', ['bundle'])
          ->condition('p.item_id', $entityId)
          ->execute();
        $rows = $query->fetchAll();
        $row = reset($rows);
        if ($row) {
          return [
            'bundle' => $row->bundle,
            'label' => "Paragraph $entityId",
          ];
        }
        else {
          return [
            'bundle' => 'unknown',
            'label' => 'unknown',
          ];
        }

      default:
        return [
          'bundle' => 'unknown',
          'label' => 'unknown',
        ];
    }
  }


  // Secondary entities are things like paragraphs, field_collection
  // that don't have their own page.

  public function title(string $entityType, string $bundle, string $fieldName) {
    switch ($entityType) {
      case 'paragraphs_item':
        return t('Usage of %field in %bundle paragraphs', [
          '%field' => $fieldName,
          '%bundle' => $bundle,
        ]);
      default:
        return t('Usage of %field in entities of type %type and bundle %bundle', [
          '%field' => $fieldName,
          '%type' => $entityType,
          '%bundle' => $bundle,
        ]);
    }
  }


  public function page(string $entityType, string $bundle, string $fieldName) {
    $con = Database::getConnection('default', 'migrate');
    $fieldType = $this->d7FieldTypes->getFieldType($entityType, $fieldName);
    $ancestry = new Ancestry();
    $ancestry->load($con);

    $build['content'] = [
      '#type' => 'table',
      '#header' => [
        'Node ID',
        'Title',
        'Status',
        'Hierarchy',
        Helper::niceEntityType($entityType) . ' ID',
        'Field',
        'Type',
        'Values',
      ],
    ];

    $fieldDataTable = "field_data_{$fieldName}";

    $query = $con->select($fieldDataTable, 'f');
    $query->addField('f', 'entity_type');
    $query->addField('f', 'entity_id');
    $query->groupBy('f.entity_type');
    $query->groupBy('f.entity_id');
    $query->addExpression('COUNT(*)', 'count');
    $query->condition('f.entity_type', $entityType);
    $query->condition('f.bundle', $bundle);

    $query = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit(10);


    // Try to get 'values' as a comma separated list if we can.
    // How we do this (and whether we can or can't) varies by field type.
    // TODO: if we can use mariadb 10.5 then it would be better to use JSON_ARRAYAGG than GROUP_CONCAT.
    if ($fieldType === 'paragraphs') {
      $valueColumn = "{$fieldName}_value";
      $query->leftJoin('paragraphs_item', 'p', "p.item_id=f.{$valueColumn}");
      $valueExpression = "CONCAT('Paragraph ', p.item_id, ' (', p.bundle, ')')";
      $query->addExpression("GROUP_CONCAT($valueExpression)", 'values');
    }

    $rows = $query->execute()->fetchAll();
    foreach ($rows as $delta => $row) {
      $values = isset($row->values) ? explode(',', $row->values) : [];

      // To get the top level entity, we need to use the ancestry.
      $lineage = $ancestry->getLineageOfEntity($row->entity_type, $row->entity_id);
      $ancestors = Ancestor::parseAsArray($lineage);

      if (!empty($ancestors)) {
        $ancestors[0]->load($con);
      }

      /** @var Ancestor $node */
      $node = (!empty($ancestors) && $ancestors[0]->entityType === 'node')
        ? $ancestors[0]
        : NULL;

      // First column: Node ID
      $build['content'][$delta][] = $this->renderAncestorId($node);

      // Second column: Node title
      $build['content'][$delta][] = $this->renderAncestorTitle($node);

      // Third column: Status
      $build['content'][$delta][] = [
        '#plain_text' => ($node ? ($node->published ? 'Published' : 'Not published') : ''),
      ];

      // Fourth column: hierarchy
      $build['content'][$delta][] = $this->renderAncestors($ancestors);

      // Fifth column: entity ID
      $build['content'][$delta][] = $this->renderEntityId($entityType, $row->entity_id);

      $build['content'][$delta][] = [
        '#plain_text' => $fieldName,
      ];

      $build['content'][$delta][] = [
        '#plain_text' => $fieldType,
      ];

      if ($values) {
        $build['content'][$delta][] = [
          '#theme' => 'item_list',
          '#items' => $values,
        ];
      }
      else {
        $build['content'][$delta][] = [
          '#plain_text' => $this->formatPlural($row->count, '1 value', '@count values'),
        ];
      }

    }

    $build['pager'] = [
      '#type' => 'pager',
    ];

    return $build;
  }


  /**
   * @param Ancestor[] $ancestors
   */
  protected function renderAncestors(array $ancestors) {
    $output = [];

    foreach ($ancestors as $ancestor) {
      $output[] = [
        '#plain_text' => Helper::niceEntityTypeAndBundleAndId($ancestor->entityType, $ancestor->bundle, $ancestor->entityId),
        '#suffix' => ' ',
      ];
      if ($ancestor->fieldName) {
        $output[] = [
          '#plain_text' => "has {$ancestor->fieldName}:",
          '#suffix' => '<br>',
        ];
      }
    }

    return $output;
  }



  protected function fetchPreviewValues(Connection $con, string $entityType, int $entityId, string $fieldName, string $fieldType): array {
    $values = [];
    $fieldDataTable = "field_data_{$fieldName}";

    $query = $con->select($fieldDataTable, 'f');
    $query->condition('f.entity_type', $entityType);
    $query->condition('f.entity_id', $entityId);

    switch ($fieldType) {
      case 'paragraphs':
        $valueColumn = "{$fieldName}_value";
        $query->leftJoin('paragraphs_item', 'p', "p.item_id=f.{$valueColumn}");
        $query->addExpression("CONCAT('Paragraph ', p.item_id, ' (', p.bundle, ')')", 'value');
        break;
      case 'text_long':
        $valueColumn = "{$fieldName}_value";
        $query->addExpression("LEFT(f.{$valueColumn}, 20)", 'value');
      default:
        $query->addExpression("'?'", 'value');
    }

    foreach ($query->execute()->fetchCol() as $value) {
      if ($fieldType === 'text_long') {
        $value = substr(strip_tags($value), 0, 20);
      }
      $values[] = $value;
    }

    return $values;
  }


}
