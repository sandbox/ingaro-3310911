<?php

namespace Drupal\d7_field_analysis\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\d7_field_analysis\Ancestor;
use Drupal\migrate_expanded\D7FieldTypes;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class FieldUsageControllerBase extends ControllerBase {

  protected D7FieldTypes $d7FieldTypes;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->d7FieldTypes = $container->get('migrate_expanded.d7_field_types');
    return $instance;
  }


  protected function renderEntityId(string $entityType, int $entityId): array {
    return [
      '#type' => 'link',
      '#title' => $entityId,
      '#url' => Url::fromRoute('d7_field_analysis.entity', [
        'entityType' => $entityType,
        'entityId' => $entityId,
      ]),
    ];
  }

  protected function renderAncestorId(?Ancestor $ancestor): array {
    $build = [];
    if ($ancestor) {
      $build[] = [
        '#type' => 'link',
        '#title' => $ancestor->entityId,
        '#url' => Url::fromRoute('d7_field_analysis.entity', [
          'entityType' => $ancestor->entityType,
          'entityId' => $ancestor->entityId,
        ]),
      ];
    }
    return $build;
  }

  protected function renderAncestorTitle(?Ancestor $ancestor): array {
    return $ancestor
      ? $this->renderTitle($ancestor->title, $ancestor->path)
      : [];
  }

  protected function renderTitle(?string $title, ?string $path): array {
    $build = [];
    if ($title) {
      $build[] = [
        '#plain_text' => $title,
        '#suffix' => '<br>',
      ];
    }
    return $build;
  }

  protected function renderValues(int $count, ?array $values): array {
    if ($values) {
      return [
        '#theme' => 'item_list',
        '#items' => $values,
      ];
    }
    else {
      return [
        '#plain_text' => $this->formatPlural($count, '1 value', '@count values'),
      ];
    }
  }

}
