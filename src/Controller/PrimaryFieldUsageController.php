<?php

namespace Drupal\d7_field_analysis\Controller;

use Drupal\Core\Database\Database;
use Drupal\d7_field_analysis\Helper;

class PrimaryFieldUsageController extends FieldUsageControllerBase {


  public function title(string $entityType, string $bundle, string $fieldName) {
    switch ($entityType) {
      case 'node':
        return t('Usage of %field in %bundle nodes', [
          '%field' => $fieldName,
          '%bundle' => $bundle,
        ]);
      default:
        return t('Usage of %field in entities of type %type and bundle %bundle', [
          '%field' => $fieldName,
          '%type' => $entityType,
          '%bundle' => $bundle,
        ]);
    }
  }



  public function page(string $entityType, string $bundle, string $fieldName) {
    $con = Database::getConnection('default', 'migrate');
    $fieldType = $this->d7FieldTypes->getFieldType($entityType, $fieldName);

    $columnHeadings = [];
    $columnHeadings[] = Helper::niceEntityType($entityType) . ' ID';
    $columnHeadings[] = 'Title';
    $columnHeadings[] = 'Status';
    $columnHeadings[] = 'Field';
    $columnHeadings[] = 'Values';

    $build['content'] = [
      '#type' => 'table',
      '#header' => $columnHeadings,
    ];


    $fieldDataTable = "field_data_{$fieldName}";

    $query = $con->select($fieldDataTable, 'f');
    $query->addField('f', 'entity_type');
    $query->addField('f', 'entity_id');
    $query->groupBy('f.entity_type');
    $query->groupBy('f.entity_id');
    $query->addExpression('COUNT(*)', 'count');
    $query->condition('f.entity_type', $entityType);
    $query->condition('f.bundle', $bundle);

    // If possible, make 'title', 'status' and 'path' available in the query results.
    // How we get these (and whether we can or can't) varies by entity type.
    if ($entityType === 'node') {
      $query->innerJoin('node', 'n', 'n.nid=f.entity_id');
      $query->addField('n', 'title', 'label');
      $query->addField('n', 'status');
      $query->addExpression("CONCAT('/node/', n.nid)", 'path');
    }
    elseif ($entityType === 'taxonomy_term') {
      $query->innerJoin('taxonomy_term_data', 't', 't.tid=f.entity_id');
      $query->addField('t', 'name', 'label');
      $query->addExpression("CONCAT('/taxonomy/term/', t.tid)", 'path');
    }

    // Try to get 'values' as a comma separated list if we can.
    // How we do this (and whether we can or can't) varies by field type.
    // TODO: if we can use mariadb 10.5 then it would be better to use JSON_ARRAYAGG than GROUP_CONCAT.
    if ($fieldType === 'paragraphs') {
      $valueColumn = "{$fieldName}_value";
      $query->leftJoin('paragraphs_item', 'p', "p.item_id=f.{$valueColumn}");
      $valueExpression = "CONCAT('Paragraph ', p.item_id, ' (', p.bundle, ')')";
      $query->addExpression("GROUP_CONCAT($valueExpression)", 'values');
    }

    $query = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit(10);

    $rows = $query->execute()->fetchAll();
    foreach ($rows as $delta => $row) {

      $build['content'][$delta][] = $this->renderEntityId($entityType, $row->entity_id);

      $build['content'][$delta][] = $this->renderTitle($row->label, $row->path);

      $build['content'][$delta][] = [
        '#plain_text' => isset($row->status) ? ($row->status ? 'Published' : 'Not published') : '',
      ];

      $build['content'][$delta][] = [
        '#plain_text' => $fieldName,
      ];

      $values = isset($row->values) ? explode(',', $row->values) : [];
      $build['content'][$delta][] = $this->renderValues($row->count, $values);
    }

    $build['pager'] = [
      '#type' => 'pager',
    ];

    return $build;
  }



}
