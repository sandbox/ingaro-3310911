<?php

namespace Drupal\d7_field_analysis\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\d7_field_analysis\Helper;
use Drupal\migrate_expanded\D7EntityLoader;
use Symfony\Component\HttpFoundation\JsonResponse;

class D7EntityController extends ControllerBase {

  public function title(string $entityType, int $entityId, int $revisionId = NULL): string {
    return $revisionId
      ? Helper::niceEntityTypeAndRevision($entityType, $entityId, $revisionId)
      : Helper::niceEntityTypeAndId($entityType, $entityId);
  }

  public function page(string $entityType, int $entityId, int $revisionId = NULL) {
    /** @var D7EntityLoader $entityLoader */
    $entityLoader = \Drupal::service('migrate_expanded.d7_entity_loader');
    $data = $entityLoader->load($entityType, $entityId, $revisionId);
    return new JsonResponse($data);
  }

}
