<?php

namespace Drupal\d7_field_analysis;

use Drupal\Core\Database\Connection;

class Ancestor {

  public string $entityType;
  public string $bundle;
  public int $entityId;
  public ?string $fieldName;
  public ?Ancestor $child;

  public string $title;
  public bool $published;
  public string $path;



  public function __construct(string $entityType, string $bundle, int $entityId) {
    $this->entityType = $entityType;
    $this->bundle = $bundle;
    $this->entityId = $entityId;

    if ($this->entityType === 'node') {
      $this->path = "/node/$entityId";
    }
  }


  public static function parse(string $lineage): ?Ancestor {
    if (empty($lineage)) {
      return NULL;
    }

    $name = "[0-9a-z_]+";
    $id = "[0-9]+";
    $dot = '\.';
    $bar = '\|';

    $matchEntity = "({$name}){$dot}({$name}){$dot}({$id})";


    $pattern = "/^{$matchEntity}(?:{$dot}({$name}))?{$bar}?(.*)$/";

    $matches = [];
    if (!preg_match($pattern, $lineage, $matches)) {
      throw new \InvalidArgumentException($lineage);
    }

    $entityType = $matches[1];
    $bundle = $matches[2];
    $entityId = (int) $matches[3];
    $instance = new static($entityType, $bundle, $entityId);

    if (count($matches) > 5) {
      $instance->fieldName = $matches[4];
      $instance->child = self::parse($matches[5]);
    }

    return $instance;
  }


  /**
   * @return Ancestor[]
   */
  public static function parseAsArray(string $lineage): array {
    $result = [];
    $ancestor = self::parse($lineage);
    while ($ancestor) {
      $result[] = $ancestor;
      $ancestor = $ancestor->child;
    }
    return $result;
  }


  public function load(Connection $con): void {
    if ($this->entityType === 'node') {
      $query = $con->select('node', 'n')
        ->fields('n', ['title', 'status'])
        ->condition('n.nid', $this->entityId);
      foreach ($query->execute()->fetchAll() as $row) {
        $this->title = $row->title;
        $this->published = $row->status;
      }
    }
  }


}
