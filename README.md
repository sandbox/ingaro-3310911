# D7 field analysis

## D7EntityViewer controller

You can view the structure of a complete entity in the `migrate` database
at `/d7-entity/{entityType}/{entityId}` or
`/d7-entity/{entityType}/{entityId}/{revisionId}`. This uses the
`D7EntityLoader` service above, so you can see what data is available to
the migration.

Because this URL exposes data directly from the D7 database, without
regard to any access control in that site, you need to be an
administrator to use it.
